=begin
#PriceFinder API v1.5.0

#Pricefinder API provides access to property, sale and listing searches as well as suburb statistics and SSO to the Pricefinder website capabilities.

OpenAPI spec version: ${build.version}

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.2.3

=end

require 'date'

module Pricefinder

  class Sale
    attr_accessor :id

    attr_accessor :disclaimer

    attr_accessor :messages

    attr_accessor :price

    attr_accessor :sale_date

    attr_accessor :settlement_date

    attr_accessor :contract_date

    attr_accessor :image

    attr_accessor :property

    attr_accessor :street

    attr_accessor :suburb

    attr_accessor :property_type

    attr_accessor :sale_type

    attr_accessor :agency

    attr_accessor :agent

    attr_accessor :location

    attr_accessor :metadata

    attr_accessor :address

    attr_accessor :property_features

    attr_accessor :land_details

    attr_accessor :rpd

    attr_accessor :dealing_number

    attr_accessor :extra

    attr_accessor :sale_participants

    attr_accessor :_self


    # Attribute mapping from ruby-style variable name to JSON key.
    def self.attribute_map
      {
        :'id' => :'id',
        :'disclaimer' => :'disclaimer',
        :'messages' => :'messages',
        :'price' => :'price',
        :'sale_date' => :'saleDate',
        :'settlement_date' => :'settlementDate',
        :'contract_date' => :'contractDate',
        :'image' => :'image',
        :'property' => :'property',
        :'street' => :'street',
        :'suburb' => :'suburb',
        :'property_type' => :'propertyType',
        :'sale_type' => :'saleType',
        :'agency' => :'agency',
        :'agent' => :'agent',
        :'location' => :'location',
        :'metadata' => :'metadata',
        :'address' => :'address',
        :'property_features' => :'propertyFeatures',
        :'land_details' => :'landDetails',
        :'rpd' => :'rpd',
        :'dealing_number' => :'dealingNumber',
        :'extra' => :'extra',
        :'sale_participants' => :'saleParticipants',
        :'_self' => :'_self'
      }
    end

    # Attribute type mapping.
    def self.swagger_types
      {
        :'id' => :'Integer',
        :'disclaimer' => :'String',
        :'messages' => :'Array<Message>',
        :'price' => :'SensitivePrice',
        :'sale_date' => :'SensitiveDate',
        :'settlement_date' => :'SensitiveDate',
        :'contract_date' => :'SensitiveDate',
        :'image' => :'ImageIdentifier',
        :'property' => :'PropertyIdentifier',
        :'street' => :'StreetIdentifier',
        :'suburb' => :'SuburbIdentifier',
        :'property_type' => :'String',
        :'sale_type' => :'String',
        :'agency' => :'Agency',
        :'agent' => :'Agent',
        :'location' => :'Point',
        :'metadata' => :'SaleMetadata',
        :'address' => :'Address',
        :'property_features' => :'ExtendedPropertyFeatures',
        :'land_details' => :'LandDetails',
        :'rpd' => :'String',
        :'dealing_number' => :'String',
        :'extra' => :'SearchExtra',
        :'sale_participants' => :'SaleParticipantsSummary',
        :'_self' => :'String'
      }
    end

    # Initializes the object
    # @param [Hash] attributes Model attributes in the form of hash
    def initialize(attributes = {})
      return unless attributes.is_a?(Hash)

      # convert string to symbol for hash key
      attributes = attributes.each_with_object({}){|(k,v), h| h[k.to_sym] = v}

      if attributes.has_key?(:'id')
        self.id = attributes[:'id']
      end

      if attributes.has_key?(:'disclaimer')
        self.disclaimer = attributes[:'disclaimer']
      end

      if attributes.has_key?(:'messages')
        if (value = attributes[:'messages']).is_a?(Array)
          self.messages = value
        end
      end

      if attributes.has_key?(:'price')
        self.price = attributes[:'price']
      end

      if attributes.has_key?(:'saleDate')
        self.sale_date = attributes[:'saleDate']
      end

      if attributes.has_key?(:'settlementDate')
        self.settlement_date = attributes[:'settlementDate']
      end

      if attributes.has_key?(:'contractDate')
        self.contract_date = attributes[:'contractDate']
      end

      if attributes.has_key?(:'image')
        self.image = attributes[:'image']
      end

      if attributes.has_key?(:'property')
        self.property = attributes[:'property']
      end

      if attributes.has_key?(:'street')
        self.street = attributes[:'street']
      end

      if attributes.has_key?(:'suburb')
        self.suburb = attributes[:'suburb']
      end

      if attributes.has_key?(:'propertyType')
        self.property_type = attributes[:'propertyType']
      end

      if attributes.has_key?(:'saleType')
        self.sale_type = attributes[:'saleType']
      end

      if attributes.has_key?(:'agency')
        self.agency = attributes[:'agency']
      end

      if attributes.has_key?(:'agent')
        self.agent = attributes[:'agent']
      end

      if attributes.has_key?(:'location')
        self.location = attributes[:'location']
      end

      if attributes.has_key?(:'metadata')
        self.metadata = attributes[:'metadata']
      end

      if attributes.has_key?(:'address')
        self.address = attributes[:'address']
      end

      if attributes.has_key?(:'propertyFeatures')
        self.property_features = attributes[:'propertyFeatures']
      end

      if attributes.has_key?(:'landDetails')
        self.land_details = attributes[:'landDetails']
      end

      if attributes.has_key?(:'rpd')
        self.rpd = attributes[:'rpd']
      end

      if attributes.has_key?(:'dealingNumber')
        self.dealing_number = attributes[:'dealingNumber']
      end

      if attributes.has_key?(:'extra')
        self.extra = attributes[:'extra']
      end

      if attributes.has_key?(:'saleParticipants')
        self.sale_participants = attributes[:'saleParticipants']
      end

      if attributes.has_key?(:'_self')
        self._self = attributes[:'_self']
      end

    end

    # Show invalid properties with the reasons. Usually used together with valid?
    # @return Array for valid properies with the reasons
    def list_invalid_properties
      invalid_properties = Array.new
      return invalid_properties
    end

    # Check to see if the all the properties in the model are valid
    # @return true if the model is valid
    def valid?
      return true
    end

    # Checks equality by comparing each attribute.
    # @param [Object] Object to be compared
    def ==(o)
      return true if self.equal?(o)
      self.class == o.class &&
          id == o.id &&
          disclaimer == o.disclaimer &&
          messages == o.messages &&
          price == o.price &&
          sale_date == o.sale_date &&
          settlement_date == o.settlement_date &&
          contract_date == o.contract_date &&
          image == o.image &&
          property == o.property &&
          street == o.street &&
          suburb == o.suburb &&
          property_type == o.property_type &&
          sale_type == o.sale_type &&
          agency == o.agency &&
          agent == o.agent &&
          location == o.location &&
          metadata == o.metadata &&
          address == o.address &&
          property_features == o.property_features &&
          land_details == o.land_details &&
          rpd == o.rpd &&
          dealing_number == o.dealing_number &&
          extra == o.extra &&
          sale_participants == o.sale_participants &&
          _self == o._self
    end

    # @see the `==` method
    # @param [Object] Object to be compared
    def eql?(o)
      self == o
    end

    # Calculates hash code according to all attributes.
    # @return [Fixnum] Hash code
    def hash
      [id, disclaimer, messages, price, sale_date, settlement_date, contract_date, image, property, street, suburb, property_type, sale_type, agency, agent, location, metadata, address, property_features, land_details, rpd, dealing_number, extra, sale_participants, _self].hash
    end

    # Builds the object from hash
    # @param [Hash] attributes Model attributes in the form of hash
    # @return [Object] Returns the model itself
    def build_from_hash(attributes)
      return nil unless attributes.is_a?(Hash)
      self.class.swagger_types.each_pair do |key, type|
        if type =~ /\AArray<(.*)>/i
          # check to ensure the input is an array given that the the attribute
          # is documented as an array but the input is not
          if attributes[self.class.attribute_map[key]].is_a?(Array)
            self.send("#{key}=", attributes[self.class.attribute_map[key]].map{ |v| _deserialize($1, v) } )
          end
        elsif !attributes[self.class.attribute_map[key]].nil?
          self.send("#{key}=", _deserialize(type, attributes[self.class.attribute_map[key]]))
        end # or else data not found in attributes(hash), not an issue as the data can be optional
      end

      self
    end

    # Deserializes the data based on type
    # @param string type Data type
    # @param string value Value to be deserialized
    # @return [Object] Deserialized data
    def _deserialize(type, value)
      case type.to_sym
      when :DateTime
        DateTime.parse(value)
      when :Date
        Date.parse(value)
      when :String
        value.to_s
      when :Integer
        value.to_i
      when :Float
        value.to_f
      when :BOOLEAN
        if value.to_s =~ /\A(true|t|yes|y|1)\z/i
          true
        else
          false
        end
      when :Object
        # generic object (usually a Hash), return directly
        value
      when /\AArray<(?<inner_type>.+)>\z/
        inner_type = Regexp.last_match[:inner_type]
        value.map { |v| _deserialize(inner_type, v) }
      when /\AHash<(?<k_type>.+?), (?<v_type>.+)>\z/
        k_type = Regexp.last_match[:k_type]
        v_type = Regexp.last_match[:v_type]
        {}.tap do |hash|
          value.each do |k, v|
            hash[_deserialize(k_type, k)] = _deserialize(v_type, v)
          end
        end
      else # model
        temp_model = Pricefinder.const_get(type).new
        temp_model.build_from_hash(value)
      end
    end

    # Returns the string representation of the object
    # @return [String] String presentation of the object
    def to_s
      to_hash.to_s
    end

    # to_body is an alias to to_hash (backward compatibility)
    # @return [Hash] Returns the object in the form of hash
    def to_body
      to_hash
    end

    # Returns the object in the form of hash
    # @return [Hash] Returns the object in the form of hash
    def to_hash
      hash = {}
      self.class.attribute_map.each_pair do |attr, param|
        value = self.send(attr)
        next if value.nil?
        hash[param] = _to_hash(value)
      end
      hash
    end

    # Outputs non-array value in the form of hash
    # For object, use to_hash. Otherwise, just return the value
    # @param [Object] value Any valid value
    # @return [Hash] Returns the value in the form of hash
    def _to_hash(value)
      if value.is_a?(Array)
        value.compact.map{ |v| _to_hash(v) }
      elsif value.is_a?(Hash)
        {}.tap do |hash|
          value.each { |k, v| hash[k] = _to_hash(v) }
        end
      elsif value.respond_to? :to_hash
        value.to_hash
      else
        value
      end
    end

  end

end
