=begin
#PriceFinder API v1.5.0

#Pricefinder API provides access to property, sale and listing searches as well as suburb statistics and SSO to the Pricefinder website capabilities.

OpenAPI spec version: ${build.version}

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.2.3

=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for Pricefinder::ValuationDetails
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'ValuationDetails' do
  before do
    # run before each test
    @instance = Pricefinder::ValuationDetails.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of ValuationDetails' do
    it 'should create an instance of ValuationDetails' do
      expect(@instance).to be_instance_of(Pricefinder::ValuationDetails)
    end
  end
  describe 'test attribute "valuations"' do
    it 'should work' do
       # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end

