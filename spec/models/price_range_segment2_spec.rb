=begin
#PriceFinder API v1.5.0

#Pricefinder API provides access to property, sale and listing searches as well as suburb statistics and SSO to the Pricefinder website capabilities.

OpenAPI spec version: ${build.version}

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.2.3

=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for Pricefinder::PriceRangeSegment2
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'PriceRangeSegment2' do
  before do
    # run before each test
    @instance = Pricefinder::PriceRangeSegment2.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of PriceRangeSegment2' do
    it 'should create an instance of PriceRangeSegment2' do
      expect(@instance).to be_instance_of(Pricefinder::PriceRangeSegment2)
    end
  end
  describe 'test attribute "_begin"' do
    it 'should work' do
       # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "_end"' do
    it 'should work' do
       # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "label"' do
    it 'should work' do
       # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "count"' do
    it 'should work' do
       # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end

