=begin
#PriceFinder API v1.5.0

#Pricefinder API provides access to property, sale and listing searches as well as suburb statistics and SSO to the Pricefinder website capabilities.

OpenAPI spec version: ${build.version}

Generated by: https://github.com/swagger-api/swagger-codegen.git
Swagger Codegen version: 2.2.3

=end

require 'spec_helper'
require 'json'
require 'date'

# Unit tests for Pricefinder::RentalTimeseriesValue
# Automatically generated by swagger-codegen (github.com/swagger-api/swagger-codegen)
# Please update as you see appropriate
describe 'RentalTimeseriesValue' do
  before do
    # run before each test
    @instance = Pricefinder::RentalTimeseriesValue.new
  end

  after do
    # run after each test
  end

  describe 'test an instance of RentalTimeseriesValue' do
    it 'should create an instance of RentalTimeseriesValue' do
      expect(@instance).to be_instance_of(Pricefinder::RentalTimeseriesValue)
    end
  end
  describe 'test attribute "count"' do
    it 'should work' do
       # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "median"' do
    it 'should work' do
       # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "high"' do
    it 'should work' do
       # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

  describe 'test attribute "low"' do
    it 'should work' do
       # assertion here. ref: https://www.relishapp.com/rspec/rspec-expectations/docs/built-in-matchers
    end
  end

end

