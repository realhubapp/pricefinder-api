# Pricefinder::SuburbSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**suburb** | [**Suburb**](Suburb.md) |  | [optional] 
**house** | [**SuburbStatisticsSummary**](SuburbStatisticsSummary.md) |  | [optional] 
**unit** | [**SuburbStatisticsSummary**](SuburbStatisticsSummary.md) |  | [optional] 
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 


