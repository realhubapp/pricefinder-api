# Pricefinder::SuburbRentalsTimeseriesWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**calendar_quarterly** | [**RentalsTimeSeries**](RentalsTimeSeries.md) |  | [optional] 


