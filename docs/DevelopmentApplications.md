# Pricefinder::DevelopmentApplications

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disclaimer** | **String** |  | [optional] 
**note** | **String** |  | [optional] 
**applications** | [**Array&lt;DevelopmentApplication&gt;**](DevelopmentApplication.md) |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 


