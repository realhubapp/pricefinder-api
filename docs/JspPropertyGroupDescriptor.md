# Pricefinder::JspPropertyGroupDescriptor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**deferred_syntax_allowed_as_literal** | **String** |  | [optional] 
**trim_directive_whitespaces** | **String** |  | [optional] 
**url_patterns** | **Array&lt;String&gt;** |  | [optional] 
**el_ignored** | **String** |  | [optional] 
**page_encoding** | **String** |  | [optional] 
**scripting_invalid** | **String** |  | [optional] 
**is_xml** | **String** |  | [optional] 
**include_preludes** | **Array&lt;String&gt;** |  | [optional] 
**include_codas** | **Array&lt;String&gt;** |  | [optional] 
**default_content_type** | **String** |  | [optional] 
**error_on_undeclared_namespace** | **String** |  | [optional] 
**buffer** | **String** |  | [optional] 


