# Pricefinder::CensusValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **String** |  | [optional] 
**count** | **Integer** |  | [optional] 


