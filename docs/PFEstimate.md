# Pricefinder::PFEstimate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 
**price** | **Float** |  | [optional] 
**estimate_date** | **DateTime** |  | [optional] 
**confidence_score** | [**ConfidenceScore**](ConfidenceScore.md) |  | [optional] 
**estimate_range_minimum** | **Float** |  | [optional] 
**estimate_range_maximum** | **Float** |  | [optional] 
**fsd** | **Float** |  | [optional] 


