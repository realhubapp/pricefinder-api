# Pricefinder::SaleMetadata

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**government_sale** | **BOOLEAN** |  | [optional] [default to false]
**pending** | **BOOLEAN** |  | [optional] [default to false]


