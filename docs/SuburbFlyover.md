# Pricefinder::SuburbFlyover

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**suburb** | [**Suburb**](Suburb.md) |  | [optional] 
**statistics** | [**StatisticsWrapper**](StatisticsWrapper.md) |  | [optional] 
**demographics** | [**Demographics**](Demographics.md) |  | [optional] 
**recent_sales** | [**Sales**](Sales.md) |  | [optional] 
**recent_rentals** | [**Listings**](Listings.md) |  | [optional] 
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 


