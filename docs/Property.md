# Pricefinder::Property

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 
**address** | [**Address**](Address.md) |  | [optional] 
**features** | [**ExtendedPropertyFeatures**](ExtendedPropertyFeatures.md) |  | [optional] 
**alternative_addresses** | [**Array&lt;Address&gt;**](Address.md) |  | [optional] 
**land_details** | [**LandDetails**](LandDetails.md) |  | [optional] 
**image** | [**ImageIdentifier**](ImageIdentifier.md) |  | [optional] 
**map** | [**PropertyMapIdentifier**](PropertyMapIdentifier.md) |  | [optional] 
**street** | [**StreetIdentifier**](StreetIdentifier.md) |  | [optional] 
**suburb** | [**SuburbIdentifier**](SuburbIdentifier.md) |  | [optional] 
**location** | [**Point**](Point.md) |  | [optional] 
**type** | **String** |  | [optional] 
**rpd** | **String** |  | [optional] 
**market_status** | [**MarketStatus**](MarketStatus.md) |  | [optional] 
**last_modified** | **DateTime** |  | [optional] 
**owners** | [**OwnershipSummary**](OwnershipSummary.md) |  | [optional] 
**ownership** | [**OwnershipSummary**](OwnershipSummary.md) |  | [optional] 
**last_sale** | [**Sale**](Sale.md) |  | [optional] 
**recent_listing** | [**Listing**](Listing.md) |  | [optional] 
**recent_rental** | [**Listing**](Listing.md) |  | [optional] 
**extra** | [**SearchExtra**](SearchExtra.md) |  | [optional] 
**_self** | **String** |  | [optional] 


