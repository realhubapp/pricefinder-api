# Pricefinder::ServletConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**init_parameter_names** | [**EnumerationString**](EnumerationString.md) |  | [optional] 
**servlet_context** | [**ServletContext**](ServletContext.md) |  | [optional] 
**servlet_name** | **String** |  | [optional] 


