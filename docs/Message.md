# Pricefinder::Message

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **Integer** |  | [optional] 
**text** | **String** |  | [optional] 


