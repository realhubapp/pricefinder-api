# Pricefinder::RentalTimeSeriesValueGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**all** | [**RentalTimeseriesValue**](RentalTimeseriesValue.md) |  | [optional] 
**_1** | [**RentalTimeseriesValue**](RentalTimeseriesValue.md) |  | [optional] 
**_2** | [**RentalTimeseriesValue**](RentalTimeseriesValue.md) |  | [optional] 
**_3** | [**RentalTimeseriesValue**](RentalTimeseriesValue.md) |  | [optional] 
**_4** | [**RentalTimeseriesValue**](RentalTimeseriesValue.md) |  | [optional] 


