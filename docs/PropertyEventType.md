# Pricefinder::PropertyEventType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display** | **String** |  | [optional] 
**key** | **String** |  | [optional] 


