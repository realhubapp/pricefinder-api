# Pricefinder::SuburbSalesWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timeseries** | [**SuburbSalesTimeseriesWrapper**](SuburbSalesTimeseriesWrapper.md) |  | [optional] 
**price_range_segments** | [**PriceRangeSegments**](PriceRangeSegments.md) |  | [optional] 
**peak_selling_periods** | [**PeakSellingPeriods**](PeakSellingPeriods.md) |  | [optional] 


