# Pricefinder::MarketStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **String** |  | [optional] 
**recently_sold** | **BOOLEAN** |  | [optional] [default to false]
**recently_for_sale** | **BOOLEAN** |  | [optional] [default to false]
**recently_for_rent** | **BOOLEAN** |  | [optional] [default to false]
**for_sale** | **BOOLEAN** |  | [optional] [default to false]
**for_rent** | **BOOLEAN** |  | [optional] [default to false]


