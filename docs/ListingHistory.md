# Pricefinder::ListingHistory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**listings** | [**Array&lt;Listing&gt;**](Listing.md) |  | [optional] 


