# Pricefinder::ExtendedListing

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**rental** | **BOOLEAN** |  | [optional] [default to false]
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 
**agents** | [**Array&lt;Agent&gt;**](Agent.md) |  | [optional] 
**agencies** | [**Array&lt;Agency&gt;**](Agency.md) |  | [optional] 
**price** | [**SensitivePrice**](SensitivePrice.md) |  | [optional] 
**start_date** | [**SensitiveDate**](SensitiveDate.md) |  | [optional] 
**image** | [**ImageIdentifier**](ImageIdentifier.md) |  | [optional] 
**property** | [**PropertyIdentifier**](PropertyIdentifier.md) |  | [optional] 
**street** | [**StreetIdentifier**](StreetIdentifier.md) |  | [optional] 
**suburb** | [**SuburbIdentifier**](SuburbIdentifier.md) |  | [optional] 
**location** | [**Point**](Point.md) |  | [optional] 
**property_type** | **String** |  | [optional] 
**type** | **String** |  | [optional] 
**status** | [**ListingStatus**](ListingStatus.md) |  | [optional] 
**end_date** | [**SensitiveDate**](SensitiveDate.md) |  | [optional] 
**last_modified** | [**SensitiveDate**](SensitiveDate.md) |  | [optional] 
**property_features** | [**ExtendedPropertyFeatures**](ExtendedPropertyFeatures.md) |  | [optional] 
**address** | [**ExtendedAddress**](ExtendedAddress.md) |  | [optional] 
**land_details** | [**LandDetails**](LandDetails.md) |  | [optional] 
**_self** | **String** |  | [optional] 


