# Pricefinder::StreetsApi

All URIs are relative to *https://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**street_listings**](StreetsApi.md#street_listings) | **GET** /streets/{streetId}/listings | 
[**street_properties**](StreetsApi.md#street_properties) | **GET** /streets/{streetId}/properties | 
[**street_rentals**](StreetsApi.md#street_rentals) | **GET** /streets/{streetId}/rentals | 
[**street_sales**](StreetsApi.md#street_sales) | **GET** /streets/{streetId}/sales | 


# **street_listings**
> Listings street_listings(street_id, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::StreetsApi.new

street_id = "street_id_example" # String | 

opts = { 
  street_number_from: 789, # Integer | 
  street_number_to: 789, # Integer | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  historic: true, # BOOLEAN | 
  date_start: DateTime.parse("today-1y"), # DateTime | 
  date_end: DateTime.parse("now"), # DateTime | 
  min_price: 1.2, # Float | 
  max_price: 1.2, # Float | 
  price_gt: 1.2, # Float | 
  price_lt: 1.2, # Float | 
  limit: 100, # Integer | 
  sort: ["sort_example"] # Array<String> | can pass sort multiple times, use - to indicate descending
}

begin
  result = api_instance.street_listings(street_id, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling StreetsApi->street_listings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **street_id** | **String**|  | 
 **street_number_from** | **Integer**|  | [optional] 
 **street_number_to** | **Integer**|  | [optional] 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **historic** | **BOOLEAN**|  | [optional] 
 **date_start** | **DateTime**|  | [optional] [default to today-1y]
 **date_end** | **DateTime**|  | [optional] [default to now]
 **min_price** | **Float**|  | [optional] 
 **max_price** | **Float**|  | [optional] 
 **price_gt** | **Float**|  | [optional] 
 **price_lt** | **Float**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]
 **sort** | [**Array&lt;String&gt;**](String.md)| can pass sort multiple times, use - to indicate descending | [optional] 

### Return type

[**Listings**](Listings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **street_properties**
> Properties street_properties(street_id, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::StreetsApi.new

street_id = "street_id_example" # String | 

opts = { 
  street_number_from: 789, # Integer | 
  street_number_to: 789, # Integer | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.street_properties(street_id, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling StreetsApi->street_properties: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **street_id** | **String**|  | 
 **street_number_from** | **Integer**|  | [optional] 
 **street_number_to** | **Integer**|  | [optional] 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **street_rentals**
> Listings street_rentals(street_id, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::StreetsApi.new

street_id = "street_id_example" # String | 

opts = { 
  street_number_from: 789, # Integer | 
  street_number_to: 789, # Integer | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  historic: true, # BOOLEAN | 
  date_start: DateTime.parse("today-1y"), # DateTime | 
  date_end: DateTime.parse("now"), # DateTime | 
  min_price: 1.2, # Float | 
  max_price: 1.2, # Float | 
  price_gt: 1.2, # Float | 
  price_lt: 1.2, # Float | 
  limit: 100, # Integer | 
  sort: ["sort_example"] # Array<String> | can pass sort multiple times, use - to indicate descending
}

begin
  result = api_instance.street_rentals(street_id, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling StreetsApi->street_rentals: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **street_id** | **String**|  | 
 **street_number_from** | **Integer**|  | [optional] 
 **street_number_to** | **Integer**|  | [optional] 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **historic** | **BOOLEAN**|  | [optional] 
 **date_start** | **DateTime**|  | [optional] [default to today-1y]
 **date_end** | **DateTime**|  | [optional] [default to now]
 **min_price** | **Float**|  | [optional] 
 **max_price** | **Float**|  | [optional] 
 **price_gt** | **Float**|  | [optional] 
 **price_lt** | **Float**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]
 **sort** | [**Array&lt;String&gt;**](String.md)| can pass sort multiple times, use - to indicate descending | [optional] 

### Return type

[**Listings**](Listings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **street_sales**
> Sales street_sales(street_id, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::StreetsApi.new

street_id = "street_id_example" # String | 

opts = { 
  street_number_from: 789, # Integer | 
  street_number_to: 789, # Integer | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  date_start: DateTime.parse("today-10y"), # DateTime | 
  date_end: DateTime.parse("now"), # DateTime | 
  min_price: 1.2, # Float | 
  max_price: 1.2, # Float | 
  price_gt: 1.2, # Float | 
  price_lt: 1.2, # Float | 
  limit: 100, # Integer | 
  sort: ["sort_example"] # Array<String> | can pass sort multiple times, use - to indicate descending
}

begin
  result = api_instance.street_sales(street_id, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling StreetsApi->street_sales: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **street_id** | **String**|  | 
 **street_number_from** | **Integer**|  | [optional] 
 **street_number_to** | **Integer**|  | [optional] 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **date_start** | **DateTime**|  | [optional] [default to today-10y]
 **date_end** | **DateTime**|  | [optional] [default to now]
 **min_price** | **Float**|  | [optional] 
 **max_price** | **Float**|  | [optional] 
 **price_gt** | **Float**|  | [optional] 
 **price_lt** | **Float**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]
 **sort** | [**Array&lt;String&gt;**](String.md)| can pass sort multiple times, use - to indicate descending | [optional] 

### Return type

[**Sales**](Sales.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



