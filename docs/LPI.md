# Pricefinder::LPI

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lot** | **String** |  | [optional] 
**plan** | **String** |  | [optional] 
**section** | **String** |  | [optional] 
**part** | **String** |  | [optional] 


