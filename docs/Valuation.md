# Pricefinder::Valuation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date** | **DateTime** |  | [optional] 
**effective_date** | **DateTime** |  | [optional] 
**method** | **String** |  | [optional] 
**amount** | **Float** |  | [optional] 
**adjusted_site_value** | **Float** |  | [optional] 


