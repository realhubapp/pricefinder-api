# Pricefinder::SuburbStatisticsSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**calculated** | **DateTime** |  | [optional] 
**median_sale_price** | **Float** |  | [optional] 
**median_rental_price** | **Float** |  | [optional] 
**suburb_growth** | **Float** |  | [optional] 
**sale_count** | **Integer** |  | [optional] 
**rental_count** | **Integer** |  | [optional] 
**suburb_rental_yield** | **Float** |  | [optional] 


