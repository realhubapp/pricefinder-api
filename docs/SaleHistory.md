# Pricefinder::SaleHistory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sales** | [**Array&lt;Sale&gt;**](Sale.md) |  | [optional] 


