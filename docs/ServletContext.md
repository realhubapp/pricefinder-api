# Pricefinder::ServletContext

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**init_parameter_names** | [**EnumerationString**](EnumerationString.md) |  | [optional] 
**servlet_registrations** | [**Hash&lt;String, ServletRegistration&gt;**](ServletRegistration.md) |  | [optional] 
**filter_registrations** | [**Hash&lt;String, FilterRegistration&gt;**](FilterRegistration.md) |  | [optional] 
**session_cookie_config** | [**SessionCookieConfig**](SessionCookieConfig.md) |  | [optional] 
**default_session_tracking_modes** | **Array&lt;String&gt;** |  | [optional] 
**minor_version** | **Integer** |  | [optional] 
**class_loader** | [**ClassLoader**](ClassLoader.md) |  | [optional] 
**attribute_names** | [**EnumerationString**](EnumerationString.md) |  | [optional] 
**server_info** | **String** |  | [optional] 
**major_version** | **Integer** |  | [optional] 
**effective_major_version** | **Integer** |  | [optional] 
**effective_minor_version** | **Integer** |  | [optional] 
**jsp_config_descriptor** | [**JspConfigDescriptor**](JspConfigDescriptor.md) |  | [optional] 
**effective_session_tracking_modes** | **Array&lt;String&gt;** |  | [optional] 
**context_path** | **String** |  | [optional] 
**servlet_context_name** | **String** |  | [optional] 
**servlet_names** | [**EnumerationString**](EnumerationString.md) |  | [optional] 
**servlets** | [**EnumerationServlet**](EnumerationServlet.md) |  | [optional] 


