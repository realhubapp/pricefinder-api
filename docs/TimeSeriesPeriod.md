# Pricefinder::TimeSeriesPeriod

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**year** | **Integer** |  | [optional] 
**month** | **Integer** |  | [optional] 
**interval** | **Integer** |  | [optional] 
**count** | **Integer** |  | [optional] 


