# Pricefinder::SensitiveDate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display** | **String** |  | [optional] 
**value** | **DateTime** |  | [optional] 
**raw_value** | **DateTime** |  | [optional] 


