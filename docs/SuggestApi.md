# Pricefinder::SuggestApi

All URIs are relative to *https://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**suggest**](SuggestApi.md#suggest) | **GET** /suggest | 
[**suggest_properties**](SuggestApi.md#suggest_properties) | **GET** /suggest/properties | 
[**suggest_streets**](SuggestApi.md#suggest_streets) | **GET** /suggest/streets | 
[**suggest_suburbs**](SuggestApi.md#suggest_suburbs) | **GET** /suggest/suburbs | 


# **suggest**
> SuggestMatches suggest(q, opts)



Provides address suggestions for the given search query (the q parameter)    The suggest expects partial addresses in the form '1A/12-34 Sample St, Suburb, QLD, 4000'  but the search will also attempt to find matches for informal queries.    For example the search \"Red Hill\" will result in search for the road Red Hill, but will also look for roads with the name 'Red Hill' with any street type and suburbs beginning with Red Hill.    The sub paths to suggestions restrict the possible return types (suburbs, streets, properties).    The suggestions returned have ids that can be used to access the rest of the api.    The q query parameter is required. The minimum query length is 2 characters.

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuggestApi.new

q = "q_example" # String | Address search query

opts = { 
  match_ids: false # BOOLEAN | 
}

begin
  result = api_instance.suggest(q, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuggestApi->suggest: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| Address search query | 
 **match_ids** | **BOOLEAN**|  | [optional] [default to false]

### Return type

[**SuggestMatches**](SuggestMatches.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **suggest_properties**
> SuggestMatches suggest_properties(q, opts)



See <a class=\"shebang\" href=\"#!/suggest/suggest\">suggestions</a>

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuggestApi.new

q = "q_example" # String | Address search query

opts = { 
  match_ids: false # BOOLEAN | 
}

begin
  result = api_instance.suggest_properties(q, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuggestApi->suggest_properties: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| Address search query | 
 **match_ids** | **BOOLEAN**|  | [optional] [default to false]

### Return type

[**SuggestMatches**](SuggestMatches.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **suggest_streets**
> SuggestMatches suggest_streets(q, opts)



See <a class=\"shebang\" href=\"#!/suggest/suggest\">suggestions</a>

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuggestApi.new

q = "q_example" # String | Address search query

opts = { 
  match_ids: false # BOOLEAN | 
}

begin
  result = api_instance.suggest_streets(q, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuggestApi->suggest_streets: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| Address search query | 
 **match_ids** | **BOOLEAN**|  | [optional] [default to false]

### Return type

[**SuggestMatches**](SuggestMatches.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **suggest_suburbs**
> SuggestMatches suggest_suburbs(q, opts)



See <a class=\"shebang\" href=\"#!/suggest/suggest\">suggestions</a>

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuggestApi.new

q = "q_example" # String | Address search query

opts = { 
  match_ids: false # BOOLEAN | 
}

begin
  result = api_instance.suggest_suburbs(q, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuggestApi->suggest_suburbs: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| Address search query | 
 **match_ids** | **BOOLEAN**|  | [optional] [default to false]

### Return type

[**SuggestMatches**](SuggestMatches.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



