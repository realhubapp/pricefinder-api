# Pricefinder::SensitivePrice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display** | **String** |  | [optional] 
**value** | **Float** |  | [optional] 
**raw_value** | **Float** |  | [optional] 
**withheld** | **BOOLEAN** |  | [optional] [default to false]


