# Pricefinder::PriceRangeSegments

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**calculated** | **DateTime** |  | [optional] 
**segments** | [**Array&lt;PriceRangeSegment&gt;**](PriceRangeSegment.md) |  | [optional] 
**period** | [**TimeSeriesPeriod**](TimeSeriesPeriod.md) |  | [optional] 
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 


