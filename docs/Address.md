# Pricefinder::Address

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**street** | **String** |  | [optional] 
**unit** | **String** |  | [optional] 
**level** | **String** |  | [optional] 
**lot** | **String** |  | [optional] 
**street_number** | **String** |  | [optional] 
**building_name** | **String** |  | [optional] 
**street_address** | **String** |  | [optional] 
**locality** | **String** |  | [optional] 
**street_location** | **String** |  | [optional] 
**state** | **String** |  | [optional] 
**postcode** | **String** |  | [optional] 
**country_code** | **String** |  | [optional] 
**lga** | **String** |  | [optional] 


