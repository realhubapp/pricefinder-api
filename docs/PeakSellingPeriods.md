# Pricefinder::PeakSellingPeriods

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**calculated** | **DateTime** |  | [optional] 
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 
**period** | [**TimeSeriesPeriod**](TimeSeriesPeriod.md) |  | [optional] 
**values** | [**Array&lt;StatisticDouble&gt;**](StatisticDouble.md) |  | [optional] 


