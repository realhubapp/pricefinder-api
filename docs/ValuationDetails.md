# Pricefinder::ValuationDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**valuations** | [**Array&lt;Valuation&gt;**](Valuation.md) |  | [optional] 


