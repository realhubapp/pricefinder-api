# Pricefinder::SuburbRentalsByBedroomStatistics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**two** | [**SuburbRentalsTimeseriesWrapper**](SuburbRentalsTimeseriesWrapper.md) |  | [optional] 
**three** | [**SuburbRentalsTimeseriesWrapper**](SuburbRentalsTimeseriesWrapper.md) |  | [optional] 
**four_plus** | [**SuburbRentalsTimeseriesWrapper**](SuburbRentalsTimeseriesWrapper.md) |  | [optional] 


