# Pricefinder::PriceRangeSegment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_begin** | **Float** |  | [optional] 
**_end** | **Float** |  | [optional] 
**label** | **String** |  | [optional] 
**sales_by_month** | [**YearByMonthStatisticsLong**](YearByMonthStatisticsLong.md) |  | [optional] 


