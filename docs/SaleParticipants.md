# Pricefinder::SaleParticipants

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vendors_summary** | **String** |  | [optional] 
**purchasers_summary** | **String** |  | [optional] 
**vendors** | [**Array&lt;Vendor&gt;**](Vendor.md) |  | [optional] 
**purchasers** | [**Array&lt;Purchaser&gt;**](Purchaser.md) |  | [optional] 


