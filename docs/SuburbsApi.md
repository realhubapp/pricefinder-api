# Pricefinder::SuburbsApi

All URIs are relative to *https://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**demographics**](SuburbsApi.md#demographics) | **GET** /suburbs/{suburbId}/demographics | 
[**flyover**](SuburbsApi.md#flyover) | **GET** /suburbs/{suburbId}/flyover | 
[**flyover_report**](SuburbsApi.md#flyover_report) | **GET** /suburbs/{suburbId}/flyover/pdf | 
[**listings**](SuburbsApi.md#listings) | **GET** /suburbs/{suburbId}/listings | 
[**peak_selling_periods**](SuburbsApi.md#peak_selling_periods) | **GET** /suburbs/{suburbId}/stats/peaksellingperiods/{propertyType} | 
[**price_segment_sales**](SuburbsApi.md#price_segment_sales) | **GET** /suburbs/{suburbId}/stats/pricesegmentation/sales/{propertyType} | 
[**price_segment_sales2**](SuburbsApi.md#price_segment_sales2) | **GET** /suburbs/{suburbId}/stats/timeseries/pricesegmentation/sales/{propertyType} | 
[**properties**](SuburbsApi.md#properties) | **GET** /suburbs/{suburbId}/properties | 
[**rentals**](SuburbsApi.md#rentals) | **GET** /suburbs/{suburbId}/rentals | 
[**sales**](SuburbsApi.md#sales) | **GET** /suburbs/{suburbId}/sales | 
[**suburb**](SuburbsApi.md#suburb) | **GET** /suburbs/{suburbId} | 
[**summary**](SuburbsApi.md#summary) | **GET** /suburbs/{suburbId}/summary | 
[**time_series_sales**](SuburbsApi.md#time_series_sales) | **GET** /suburbs/{suburbId}/stats/timeseries/sales/{propertyType} | 


# **demographics**
> Demographics demographics(suburb_id)



Demographics (Census) data for the suburb    Topics:  <ul>   <li>Age of Population</li>   <li>Family Composition</li>   <li>Household Income - Weekly</li>   <li>Home Loan Repayments - Monthly</li>   <li>Rent Payments - Weekly</li>   <li>Dwelling Structure</li>   <li>Home Ownership</li>   <li>Non-School Qualification: Level of Education</li>   <li>Employment</li>   <li>Occupation</li>   <li>Method of Travel to Work</li>  </ul>

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuburbsApi.new

suburb_id = "suburb_id_example" # String | 


begin
  result = api_instance.demographics(suburb_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuburbsApi->demographics: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suburb_id** | **String**|  | 

### Return type

[**Demographics**](Demographics.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **flyover**
> SuburbFlyover flyover(suburb_id)



Generate a 'flyover' of the suburb. A flyover is a summary of the recent sales and rental activity.

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuburbsApi.new

suburb_id = "suburb_id_example" # String | 


begin
  result = api_instance.flyover(suburb_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuburbsApi->flyover: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suburb_id** | **String**|  | 

### Return type

[**SuburbFlyover**](SuburbFlyover.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **flyover_report**
> flyover_report(suburb_id, opts)



A suburb flyover in a pdf report

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuburbsApi.new

suburb_id = "suburb_id_example" # String | 

opts = { 
  attach: true, # BOOLEAN | 
  property_type: "House" # String | 
}

begin
  api_instance.flyover_report(suburb_id, opts)
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuburbsApi->flyover_report: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suburb_id** | **String**|  | 
 **attach** | **BOOLEAN**|  | [optional] [default to true]
 **property_type** | **String**|  | [optional] [default to House]

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pdf



# **listings**
> Listings listings(suburb_id, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuburbsApi.new

suburb_id = "suburb_id_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  historic: true, # BOOLEAN | 
  date_start: DateTime.parse("today-30d"), # DateTime | 
  date_end: DateTime.parse("now"), # DateTime | 
  min_price: 1.2, # Float | 
  max_price: 1.2, # Float | 
  price_gt: 1.2, # Float | 
  price_lt: 1.2, # Float | 
  limit: 100, # Integer | 
  sort: ["sort_example"] # Array<String> | can pass sort multiple times, use - to indicate descending
}

begin
  result = api_instance.listings(suburb_id, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuburbsApi->listings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suburb_id** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **historic** | **BOOLEAN**|  | [optional] 
 **date_start** | **DateTime**|  | [optional] [default to today-30d]
 **date_end** | **DateTime**|  | [optional] [default to now]
 **min_price** | **Float**|  | [optional] 
 **max_price** | **Float**|  | [optional] 
 **price_gt** | **Float**|  | [optional] 
 **price_lt** | **Float**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]
 **sort** | [**Array&lt;String&gt;**](String.md)| can pass sort multiple times, use - to indicate descending | [optional] 

### Return type

[**Listings**](Listings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **peak_selling_periods**
> PeakSellingPeriods peak_selling_periods(suburb_id, property_type, opts)



Peak selling periods(months). The returned values are the median number of sales for the period, calculated over 3 years.

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuburbsApi.new

suburb_id = "suburb_id_example" # String | 

property_type = "property_type_example" # String | 

opts = { 
  year_end: 789, # Integer | 
  month_end: "december", # String | 
  number_of_years: 15, # Integer | 
  aggregation_period: "year" # String | 
}

begin
  result = api_instance.peak_selling_periods(suburb_id, property_type, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuburbsApi->peak_selling_periods: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suburb_id** | **String**|  | 
 **property_type** | **String**|  | 
 **year_end** | **Integer**|  | [optional] 
 **month_end** | **String**|  | [optional] [default to december]
 **number_of_years** | **Integer**|  | [optional] [default to 15]
 **aggregation_period** | **String**|  | [optional] [default to year]

### Return type

[**PeakSellingPeriods**](PeakSellingPeriods.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **price_segment_sales**
> PriceRangeSegments price_segment_sales(suburb_id, property_type)



Price segmentation of sales over 12 months from the end of the previous quarter.    Deprecated, see /stats/timeseries/pricesegments/sales

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuburbsApi.new

suburb_id = "suburb_id_example" # String | 

property_type = "property_type_example" # String | 


begin
  result = api_instance.price_segment_sales(suburb_id, property_type)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuburbsApi->price_segment_sales: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suburb_id** | **String**|  | 
 **property_type** | **String**|  | 

### Return type

[**PriceRangeSegments**](PriceRangeSegments.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **price_segment_sales2**
> PriceRangeSegments2 price_segment_sales2(suburb_id, property_type, opts)



Price segmentation of sales

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuburbsApi.new

suburb_id = "suburb_id_example" # String | 

property_type = "property_type_example" # String | 

opts = { 
  year_end: 789, # Integer | 
  month_end: "december", # String | 
  number_of_years: 15, # Integer | 
  aggregation_period: "year" # String | 
}

begin
  result = api_instance.price_segment_sales2(suburb_id, property_type, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuburbsApi->price_segment_sales2: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suburb_id** | **String**|  | 
 **property_type** | **String**|  | 
 **year_end** | **Integer**|  | [optional] 
 **month_end** | **String**|  | [optional] [default to december]
 **number_of_years** | **Integer**|  | [optional] [default to 15]
 **aggregation_period** | **String**|  | [optional] [default to year]

### Return type

[**PriceRangeSegments2**](PriceRangeSegments2.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties**
> Properties properties(suburb_id, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuburbsApi.new

suburb_id = "suburb_id_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties(suburb_id, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuburbsApi->properties: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suburb_id** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **rentals**
> Listings rentals(suburb_id, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuburbsApi.new

suburb_id = "suburb_id_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  historic: true, # BOOLEAN | 
  date_start: DateTime.parse("today-30d"), # DateTime | 
  date_end: DateTime.parse("now"), # DateTime | 
  min_price: 1.2, # Float | 
  max_price: 1.2, # Float | 
  price_gt: 1.2, # Float | 
  price_lt: 1.2, # Float | 
  limit: 100, # Integer | 
  sort: ["sort_example"] # Array<String> | can pass sort multiple times, use - to indicate descending
}

begin
  result = api_instance.rentals(suburb_id, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuburbsApi->rentals: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suburb_id** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **historic** | **BOOLEAN**|  | [optional] 
 **date_start** | **DateTime**|  | [optional] [default to today-30d]
 **date_end** | **DateTime**|  | [optional] [default to now]
 **min_price** | **Float**|  | [optional] 
 **max_price** | **Float**|  | [optional] 
 **price_gt** | **Float**|  | [optional] 
 **price_lt** | **Float**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]
 **sort** | [**Array&lt;String&gt;**](String.md)| can pass sort multiple times, use - to indicate descending | [optional] 

### Return type

[**Listings**](Listings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **sales**
> Sales sales(suburb_id, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuburbsApi.new

suburb_id = "suburb_id_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  date_start: DateTime.parse("today-6m"), # DateTime | 
  date_end: DateTime.parse("now"), # DateTime | 
  min_price: 1.2, # Float | 
  max_price: 1.2, # Float | 
  price_gt: 1.2, # Float | 
  price_lt: 1.2, # Float | 
  limit: 100, # Integer | 
  sort: ["sort_example"] # Array<String> | can pass sort multiple times, use - to indicate descending
}

begin
  result = api_instance.sales(suburb_id, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuburbsApi->sales: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suburb_id** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **date_start** | **DateTime**|  | [optional] [default to today-6m]
 **date_end** | **DateTime**|  | [optional] [default to now]
 **min_price** | **Float**|  | [optional] 
 **max_price** | **Float**|  | [optional] 
 **price_gt** | **Float**|  | [optional] 
 **price_lt** | **Float**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]
 **sort** | [**Array&lt;String&gt;**](String.md)| can pass sort multiple times, use - to indicate descending | [optional] 

### Return type

[**Sales**](Sales.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **suburb**
> Suburb suburb(suburb_id)



Get the basic suburb information

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuburbsApi.new

suburb_id = "suburb_id_example" # String | 


begin
  result = api_instance.suburb(suburb_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuburbsApi->suburb: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suburb_id** | **String**|  | 

### Return type

[**Suburb**](Suburb.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **summary**
> SuburbSummary summary(suburb_id)



Rolling year sales statistics summary (mean, median etc) for the current year;

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuburbsApi.new

suburb_id = "suburb_id_example" # String | 


begin
  result = api_instance.summary(suburb_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuburbsApi->summary: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suburb_id** | **String**|  | 

### Return type

[**SuburbSummary**](SuburbSummary.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **time_series_sales**
> SalesTimeSeries time_series_sales(suburb_id, property_type, opts)



Calendar year timeseries for sales in the suburb

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SuburbsApi.new

suburb_id = "suburb_id_example" # String | 

property_type = "property_type_example" # String | 

opts = { 
  year_end: 789, # Integer | 
  month_end: "december", # String | 
  number_of_years: 15, # Integer | 
  aggregation_period: "year", # String | 
  year_from: 789, # Integer | 
  year_to: 789 # Integer | 
}

begin
  result = api_instance.time_series_sales(suburb_id, property_type, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SuburbsApi->time_series_sales: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **suburb_id** | **String**|  | 
 **property_type** | **String**|  | 
 **year_end** | **Integer**|  | [optional] 
 **month_end** | **String**|  | [optional] [default to december]
 **number_of_years** | **Integer**|  | [optional] [default to 15]
 **aggregation_period** | **String**|  | [optional] [default to year]
 **year_from** | **Integer**|  | [optional] 
 **year_to** | **Integer**|  | [optional] 

### Return type

[**SalesTimeSeries**](SalesTimeSeries.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



