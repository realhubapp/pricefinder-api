# Pricefinder::StatisticLong

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** |  | [optional] 
**value** | **Integer** |  | [optional] 


