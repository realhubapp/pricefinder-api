# Pricefinder::ExtendedLandDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**property_area** | **Float** |  | [optional] 
**building_area** | **Float** |  | [optional] 
**frontage** | **Float** |  | [optional] 
**parcels** | [**Array&lt;Parcel&gt;**](Parcel.md) |  | [optional] 
**primary_land_use** | [**LandUse**](LandUse.md) |  | [optional] 
**secondary_land_use** | [**LandUse**](LandUse.md) |  | [optional] 


