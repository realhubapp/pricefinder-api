# Pricefinder::SsoApi

All URIs are relative to *https://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**appraisals_rental_cma**](SsoApi.md#appraisals_rental_cma) | **GET** /sso/properties/{propertyId}/appraisals/rentalcma | 
[**appraisals_sale_cma**](SsoApi.md#appraisals_sale_cma) | **GET** /sso/properties/{propertyId}/appraisals/salecma | 
[**appraisals_soi**](SsoApi.md#appraisals_soi) | **GET** /sso/properties/{propertyId}/appraisals/soi | 
[**cma**](SsoApi.md#cma) | **GET** /sso/properties/{propertyId}/cma | 
[**home**](SsoApi.md#home) | **GET** /sso | 
[**property**](SsoApi.md#property) | **GET** /sso/properties/{propertyId} | 
[**radial_sales**](SsoApi.md#radial_sales) | **GET** /sso/properties/{propertyId}/radial/sales | 
[**titles**](SsoApi.md#titles) | **GET** /sso/properties/{propertyId}/titles | 


# **appraisals_rental_cma**
> SSOLink appraisals_rental_cma(property_id)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SsoApi.new

property_id = 789 # Integer | 


begin
  result = api_instance.appraisals_rental_cma(property_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SsoApi->appraisals_rental_cma: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 

### Return type

[**SSOLink**](SSOLink.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **appraisals_sale_cma**
> SSOLink appraisals_sale_cma(property_id)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SsoApi.new

property_id = 789 # Integer | 


begin
  result = api_instance.appraisals_sale_cma(property_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SsoApi->appraisals_sale_cma: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 

### Return type

[**SSOLink**](SSOLink.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **appraisals_soi**
> SSOLink appraisals_soi(property_id)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SsoApi.new

property_id = 789 # Integer | 


begin
  result = api_instance.appraisals_soi(property_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SsoApi->appraisals_soi: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 

### Return type

[**SSOLink**](SSOLink.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **cma**
> SSOLink cma(property_id)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SsoApi.new

property_id = 789 # Integer | 


begin
  result = api_instance.cma(property_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SsoApi->cma: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 

### Return type

[**SSOLink**](SSOLink.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **home**
> SSOLink home





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SsoApi.new

begin
  result = api_instance.home
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SsoApi->home: #{e}"
end
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**SSOLink**](SSOLink.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **property**
> SSOLink property(property_id)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SsoApi.new

property_id = 789 # Integer | 


begin
  result = api_instance.property(property_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SsoApi->property: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 

### Return type

[**SSOLink**](SSOLink.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **radial_sales**
> SSOLink radial_sales(property_id)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SsoApi.new

property_id = 789 # Integer | 


begin
  result = api_instance.radial_sales(property_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SsoApi->radial_sales: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 

### Return type

[**SSOLink**](SSOLink.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **titles**
> SSOLink titles(property_id)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SsoApi.new

property_id = 789 # Integer | 


begin
  result = api_instance.titles(property_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SsoApi->titles: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 

### Return type

[**SSOLink**](SSOLink.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



