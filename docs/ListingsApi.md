# Pricefinder::ListingsApi

All URIs are relative to *https://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**listing**](ListingsApi.md#listing) | **GET** /listings/{listingId} | 


# **listing**
> ExtendedListing listing(listing_id)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ListingsApi.new

listing_id = 789 # Integer | 


begin
  result = api_instance.listing(listing_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ListingsApi->listing: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **listing_id** | **Integer**|  | 

### Return type

[**ExtendedListing**](ExtendedListing.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



