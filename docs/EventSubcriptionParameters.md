# Pricefinder::EventSubcriptionParameters

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**event_types** | **Array&lt;String&gt;** |  | [optional] 


