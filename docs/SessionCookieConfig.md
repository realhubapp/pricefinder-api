# Pricefinder::SessionCookieConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**secure** | **BOOLEAN** |  | [optional] [default to false]
**domain** | **String** |  | [optional] 
**http_only** | **BOOLEAN** |  | [optional] [default to false]
**name** | **String** |  | [optional] 
**path** | **String** |  | [optional] 
**comment** | **String** |  | [optional] 
**max_age** | **Integer** |  | [optional] 


