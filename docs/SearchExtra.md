# Pricefinder::SearchExtra

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distance** | **Float** |  | [optional] 
**name_match_type** | **String** |  | [optional] 
**matched_name** | **String** |  | [optional] 


