# Pricefinder::Listings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 
**listings** | [**Array&lt;Listing&gt;**](Listing.md) |  | [optional] 


