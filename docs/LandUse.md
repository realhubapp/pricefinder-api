# Pricefinder::LandUse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | [optional] 
**display** | **String** |  | [optional] 


