# Pricefinder::CensusTopic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**topic** | **String** |  | [optional] 
**title** | **String** |  | [optional] 
**total** | **Integer** |  | [optional] 
**data** | [**Array&lt;CensusValue&gt;**](CensusValue.md) |  | [optional] 


