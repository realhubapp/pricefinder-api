# Pricefinder::Parcel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**area** | **Float** |  | [optional] 
**zoning** | [**Zoning**](Zoning.md) |  | [optional] 
**lpi** | [**LPI**](LPI.md) |  | [optional] 
**titles** | [**Array&lt;Title&gt;**](Title.md) |  | [optional] 


