# Pricefinder::SalesApi

All URIs are relative to *https://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sale**](SalesApi.md#sale) | **GET** /sales/{saleId} | 


# **sale**
> ExtendedSale sale(sale_id)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SalesApi.new

sale_id = 789 # Integer | 


begin
  result = api_instance.sale(sale_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SalesApi->sale: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sale_id** | **Integer**|  | 

### Return type

[**ExtendedSale**](ExtendedSale.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



