# Pricefinder::SuburbIdentifier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**_self** | **String** |  | [optional] 


