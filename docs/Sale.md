# Pricefinder::Sale

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 
**price** | [**SensitivePrice**](SensitivePrice.md) |  | [optional] 
**sale_date** | [**SensitiveDate**](SensitiveDate.md) |  | [optional] 
**settlement_date** | [**SensitiveDate**](SensitiveDate.md) |  | [optional] 
**contract_date** | [**SensitiveDate**](SensitiveDate.md) |  | [optional] 
**image** | [**ImageIdentifier**](ImageIdentifier.md) |  | [optional] 
**property** | [**PropertyIdentifier**](PropertyIdentifier.md) |  | [optional] 
**street** | [**StreetIdentifier**](StreetIdentifier.md) |  | [optional] 
**suburb** | [**SuburbIdentifier**](SuburbIdentifier.md) |  | [optional] 
**property_type** | **String** |  | [optional] 
**sale_type** | **String** |  | [optional] 
**agency** | [**Agency**](Agency.md) |  | [optional] 
**agent** | [**Agent**](Agent.md) |  | [optional] 
**location** | [**Point**](Point.md) |  | [optional] 
**metadata** | [**SaleMetadata**](SaleMetadata.md) |  | [optional] 
**address** | [**Address**](Address.md) |  | [optional] 
**property_features** | [**ExtendedPropertyFeatures**](ExtendedPropertyFeatures.md) |  | [optional] 
**land_details** | [**LandDetails**](LandDetails.md) |  | [optional] 
**rpd** | **String** |  | [optional] 
**dealing_number** | **String** |  | [optional] 
**extra** | [**SearchExtra**](SearchExtra.md) |  | [optional] 
**sale_participants** | [**SaleParticipantsSummary**](SaleParticipantsSummary.md) |  | [optional] 
**_self** | **String** |  | [optional] 


