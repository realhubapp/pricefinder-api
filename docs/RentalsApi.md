# Pricefinder::RentalsApi

All URIs are relative to *https://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rental**](RentalsApi.md#rental) | **GET** /rentals/{rentalId} | 


# **rental**
> ExtendedListing rental(rental_id)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::RentalsApi.new

rental_id = 789 # Integer | 


begin
  result = api_instance.rental(rental_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling RentalsApi->rental: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **rental_id** | **Integer**|  | 

### Return type

[**ExtendedListing**](ExtendedListing.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



