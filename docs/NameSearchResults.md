# Pricefinder::NameSearchResults

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 
**sales** | [**Array&lt;Sale&gt;**](Sale.md) |  | [optional] 
**properties** | [**Array&lt;Property&gt;**](Property.md) |  | [optional] 


