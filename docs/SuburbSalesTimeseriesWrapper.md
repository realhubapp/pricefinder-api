# Pricefinder::SuburbSalesTimeseriesWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rolling12_month** | [**SalesTimeSeries**](SalesTimeSeries.md) |  | [optional] 
**calendar12_month** | [**SalesTimeSeries**](SalesTimeSeries.md) |  | [optional] 


