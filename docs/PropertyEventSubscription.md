# Pricefinder::PropertyEventSubscription

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**subscription_date** | **DateTime** |  | [optional] 
**event_types** | [**Array&lt;PropertyEventType&gt;**](PropertyEventType.md) |  | [optional] 


