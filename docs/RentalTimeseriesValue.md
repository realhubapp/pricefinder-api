# Pricefinder::RentalTimeseriesValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  | [optional] 
**median** | **Float** |  | [optional] 
**high** | **Float** |  | [optional] 
**low** | **Float** |  | [optional] 


