# Pricefinder::Suburb

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**suburb_name** | **String** |  | [optional] 
**postcode** | **Integer** |  | [optional] 
**state** | **String** |  | [optional] 
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 
**_self** | **String** |  | [optional] 


