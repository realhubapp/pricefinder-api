# Pricefinder::Agent

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | [optional] 
**phone_number** | **String** |  | [optional] 
**agency** | [**AgencyIdentifier**](AgencyIdentifier.md) |  | [optional] 


