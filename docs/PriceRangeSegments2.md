# Pricefinder::PriceRangeSegments2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**period** | [**TimeSeriesPeriod**](TimeSeriesPeriod.md) |  | [optional] 
**values** | **Array&lt;Array&lt;PriceRangeSegment2&gt;&gt;** |  | [optional] 
**calculated** | **DateTime** |  | [optional] 
**description** | **String** |  | [optional] 
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 


