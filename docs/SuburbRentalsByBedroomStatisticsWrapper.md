# Pricefinder::SuburbRentalsByBedroomStatisticsWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**beds** | [**SuburbRentalsByBedroomStatistics**](SuburbRentalsByBedroomStatistics.md) |  | [optional] 


