# Pricefinder::SuggestMatches

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**matches** | [**Array&lt;SuggestMatch&gt;**](SuggestMatch.md) |  | [optional] 
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 


