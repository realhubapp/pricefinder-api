# Pricefinder::Image

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**date** | **DateTime** |  | [optional] 
**main_image** | **BOOLEAN** |  | [optional] [default to false]
**priority** | **Integer** |  | [optional] 
**_self** | **String** |  | [optional] 


