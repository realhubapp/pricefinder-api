# Pricefinder::PropertiesApi

All URIs are relative to *https://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**events_subscribe**](PropertiesApi.md#events_subscribe) | **POST** /properties/{propertyId}/eventsubscription | 
[**events_subscription**](PropertiesApi.md#events_subscription) | **GET** /properties/{propertyId}/eventsubscription | 
[**events_un_subscribe**](PropertiesApi.md#events_un_subscribe) | **DELETE** /properties/{propertyId}/eventsubscription | 
[**images**](PropertiesApi.md#images) | **GET** /properties/{propertyId}/images | 
[**images_main**](PropertiesApi.md#images_main) | **GET** /properties/{propertyId}/images/main | 
[**map**](PropertiesApi.md#map) | **GET** /properties/{propertyId}/map | 
[**property**](PropertiesApi.md#property) | **GET** /properties/{propertyId} | 
[**property_avm_report**](PropertiesApi.md#property_avm_report) | **GET** /properties/{propertyId}/avm/pdf | 
[**property_estimate**](PropertiesApi.md#property_estimate) | **GET** /properties/{propertyId}/estimate | 
[**property_extended**](PropertiesApi.md#property_extended) | **GET** /properties/{propertyId}/extended | 
[**property_report**](PropertiesApi.md#property_report) | **GET** /properties/{propertyId}/pdf | 
[**radial_listings**](PropertiesApi.md#radial_listings) | **GET** /properties/{propertyId}/radial/listings | 
[**radial_planning_alerts**](PropertiesApi.md#radial_planning_alerts) | **GET** /properties/{propertyId}/radial/developmentapplications | 
[**radial_properties**](PropertiesApi.md#radial_properties) | **GET** /properties/{propertyId}/radial/properties | 
[**radial_rentals**](PropertiesApi.md#radial_rentals) | **GET** /properties/{propertyId}/radial/rentals | 
[**radial_sales**](PropertiesApi.md#radial_sales) | **GET** /properties/{propertyId}/radial/sales | 
[**rental_cma_pdf**](PropertiesApi.md#rental_cma_pdf) | **GET** /properties/{propertyId}/autocma/rental/pdf | 
[**sale_cma_pdf**](PropertiesApi.md#sale_cma_pdf) | **GET** /properties/{propertyId}/autocma/sale/pdf | 
[**streetview**](PropertiesApi.md#streetview) | **GET** /properties/{propertyId}/streetview | 


# **events_subscribe**
> events_subscribe(property_id, opts)



Subscribe to property event alerts (currently only available through email notifications) for this property.

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 

opts = { 
  body: Pricefinder::EventSubcriptionParameters.new # EventSubcriptionParameters | 
}

begin
  api_instance.events_subscribe(property_id, opts)
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->events_subscribe: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 
 **body** | [**EventSubcriptionParameters**](EventSubcriptionParameters.md)|  | [optional] 

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **events_subscription**
> PropertyEventSubscription events_subscription(property_id)



Get details of any event subscription on this property.    Returns 404 if there is no event subscription

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 


begin
  result = api_instance.events_subscription(property_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->events_subscription: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 

### Return type

[**PropertyEventSubscription**](PropertyEventSubscription.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **events_un_subscribe**
> events_un_subscribe(property_id)



Unsubscribe to property event alerts for this property

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 


begin
  api_instance.events_un_subscribe(property_id)
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->events_un_subscribe: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **images**
> Images images(property_id)



Return all the images for this property

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 


begin
  result = api_instance.images(property_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->images: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 

### Return type

[**Images**](Images.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **images_main**
> Image images_main(property_id)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 


begin
  result = api_instance.images_main(property_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->images_main: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 

### Return type

[**Image**](Image.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **map**
> map(property_id, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 

opts = { 
  width: 128, # Integer | 
  height: 96, # Integer | 
  zoom: "medium", # String | 
  aerial: false, # BOOLEAN | 
  legend: false, # BOOLEAN | 
  shading: false, # BOOLEAN | 
  boundary: false, # BOOLEAN | 
  dimensions: false, # BOOLEAN | 
  area: false, # BOOLEAN | 
  title: false, # BOOLEAN | 
  easements: false, # BOOLEAN | 
  property_boundary: true # BOOLEAN | 
}

begin
  api_instance.map(property_id, opts)
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->map: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 
 **width** | **Integer**|  | [optional] [default to 128]
 **height** | **Integer**|  | [optional] [default to 96]
 **zoom** | **String**|  | [optional] [default to medium]
 **aerial** | **BOOLEAN**|  | [optional] [default to false]
 **legend** | **BOOLEAN**|  | [optional] [default to false]
 **shading** | **BOOLEAN**|  | [optional] [default to false]
 **boundary** | **BOOLEAN**|  | [optional] [default to false]
 **dimensions** | **BOOLEAN**|  | [optional] [default to false]
 **area** | **BOOLEAN**|  | [optional] [default to false]
 **title** | **BOOLEAN**|  | [optional] [default to false]
 **easements** | **BOOLEAN**|  | [optional] [default to false]
 **property_boundary** | **BOOLEAN**|  | [optional] [default to true]

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/jpeg



# **property**
> Property property(property_id)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 


begin
  result = api_instance.property(property_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->property: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 

### Return type

[**Property**](Property.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **property_avm_report**
> property_avm_report(property_id, opts)



Get a avm as a pdf report.

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 

opts = { 
  attach: true # BOOLEAN | 
}

begin
  api_instance.property_avm_report(property_id, opts)
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->property_avm_report: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 
 **attach** | **BOOLEAN**|  | [optional] [default to true]

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pdf



# **property_estimate**
> PFEstimate property_estimate(property_id)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 


begin
  result = api_instance.property_estimate(property_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->property_estimate: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 

### Return type

[**PFEstimate**](PFEstimate.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **property_extended**
> ExtendedProperty property_extended(property_id)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 


begin
  result = api_instance.property_extended(property_id)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->property_extended: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 

### Return type

[**ExtendedProperty**](ExtendedProperty.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **property_report**
> property_report(property_id, opts)



Get a property report in pdf format.

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 

opts = { 
  attach: true # BOOLEAN | 
}

begin
  api_instance.property_report(property_id, opts)
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->property_report: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 
 **attach** | **BOOLEAN**|  | [optional] [default to true]

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pdf



# **radial_listings**
> Listings radial_listings(property_id, opts)



Searches in a radius around the given property

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 

opts = { 
  radius: 500.0, # Float | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  historic: true, # BOOLEAN | 
  date_start: DateTime.parse("today-30d"), # DateTime | 
  date_end: DateTime.parse("now"), # DateTime | 
  min_price: 1.2, # Float | 
  max_price: 1.2, # Float | 
  price_gt: 1.2, # Float | 
  price_lt: 1.2, # Float | 
  limit: 100, # Integer | 
  sort: ["sort_example"] # Array<String> | can pass sort multiple times, use - to indicate descending
}

begin
  result = api_instance.radial_listings(property_id, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->radial_listings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 
 **radius** | **Float**|  | [optional] [default to 500.0]
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **historic** | **BOOLEAN**|  | [optional] 
 **date_start** | **DateTime**|  | [optional] [default to today-30d]
 **date_end** | **DateTime**|  | [optional] [default to now]
 **min_price** | **Float**|  | [optional] 
 **max_price** | **Float**|  | [optional] 
 **price_gt** | **Float**|  | [optional] 
 **price_lt** | **Float**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]
 **sort** | [**Array&lt;String&gt;**](String.md)| can pass sort multiple times, use - to indicate descending | [optional] 

### Return type

[**Listings**](Listings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **radial_planning_alerts**
> DevelopmentApplications radial_planning_alerts(property_id, opts)



Searches in a radius around the given property for development applications. Source: planningalerts.org.au

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 

opts = { 
  radius: 2000, # Integer | 
  limit: 10 # Integer | 
}

begin
  result = api_instance.radial_planning_alerts(property_id, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->radial_planning_alerts: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 
 **radius** | **Integer**|  | [optional] [default to 2000]
 **limit** | **Integer**|  | [optional] [default to 10]

### Return type

[**DevelopmentApplications**](DevelopmentApplications.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **radial_properties**
> Properties radial_properties(property_id, opts)



Searches in a radius around the given property

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 

opts = { 
  radius: 500.0, # Float | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100, # Integer | 
  sort: ["sort_example"] # Array<String> | can pass sort multiple times, use - to indicate descending
}

begin
  result = api_instance.radial_properties(property_id, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->radial_properties: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 
 **radius** | **Float**|  | [optional] [default to 500.0]
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]
 **sort** | [**Array&lt;String&gt;**](String.md)| can pass sort multiple times, use - to indicate descending | [optional] 

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **radial_rentals**
> Listings radial_rentals(property_id, opts)



Searches in a radius around the given property

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 

opts = { 
  radius: 500.0, # Float | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  historic: true, # BOOLEAN | 
  date_start: DateTime.parse("today-30d"), # DateTime | 
  date_end: DateTime.parse("now"), # DateTime | 
  min_price: 1.2, # Float | 
  max_price: 1.2, # Float | 
  price_gt: 1.2, # Float | 
  price_lt: 1.2, # Float | 
  limit: 100, # Integer | 
  sort: ["sort_example"] # Array<String> | can pass sort multiple times, use - to indicate descending
}

begin
  result = api_instance.radial_rentals(property_id, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->radial_rentals: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 
 **radius** | **Float**|  | [optional] [default to 500.0]
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **historic** | **BOOLEAN**|  | [optional] 
 **date_start** | **DateTime**|  | [optional] [default to today-30d]
 **date_end** | **DateTime**|  | [optional] [default to now]
 **min_price** | **Float**|  | [optional] 
 **max_price** | **Float**|  | [optional] 
 **price_gt** | **Float**|  | [optional] 
 **price_lt** | **Float**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]
 **sort** | [**Array&lt;String&gt;**](String.md)| can pass sort multiple times, use - to indicate descending | [optional] 

### Return type

[**Listings**](Listings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **radial_sales**
> Sales radial_sales(property_id, opts)



Searches in a radius around the given property

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 

opts = { 
  radius: 500.0, # Float | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  date_start: DateTime.parse("today-1y"), # DateTime | 
  date_end: DateTime.parse("now"), # DateTime | 
  min_price: 1.2, # Float | 
  max_price: 1.2, # Float | 
  price_gt: 1.2, # Float | 
  price_lt: 1.2, # Float | 
  limit: 100, # Integer | 
  sort: ["sort_example"] # Array<String> | can pass sort multiple times, use - to indicate descending
}

begin
  result = api_instance.radial_sales(property_id, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->radial_sales: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 
 **radius** | **Float**|  | [optional] [default to 500.0]
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **date_start** | **DateTime**|  | [optional] [default to today-1y]
 **date_end** | **DateTime**|  | [optional] [default to now]
 **min_price** | **Float**|  | [optional] 
 **max_price** | **Float**|  | [optional] 
 **price_gt** | **Float**|  | [optional] 
 **price_lt** | **Float**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]
 **sort** | [**Array&lt;String&gt;**](String.md)| can pass sort multiple times, use - to indicate descending | [optional] 

### Return type

[**Sales**](Sales.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **rental_cma_pdf**
> rental_cma_pdf(property_id, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 

opts = { 
  cover_page: true, # BOOLEAN | 
  cover_letter: false, # BOOLEAN | 
  property_report: true, # BOOLEAN | 
  comparable_rentals: true, # BOOLEAN | 
  demographics: true, # BOOLEAN | 
  stats: true, # BOOLEAN | 
  maps: true, # BOOLEAN | 
  summary: false, # BOOLEAN | 
  radius: 500, # Integer | 
  attach: true # BOOLEAN | 
}

begin
  api_instance.rental_cma_pdf(property_id, opts)
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->rental_cma_pdf: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 
 **cover_page** | **BOOLEAN**|  | [optional] [default to true]
 **cover_letter** | **BOOLEAN**|  | [optional] [default to false]
 **property_report** | **BOOLEAN**|  | [optional] [default to true]
 **comparable_rentals** | **BOOLEAN**|  | [optional] [default to true]
 **demographics** | **BOOLEAN**|  | [optional] [default to true]
 **stats** | **BOOLEAN**|  | [optional] [default to true]
 **maps** | **BOOLEAN**|  | [optional] [default to true]
 **summary** | **BOOLEAN**|  | [optional] [default to false]
 **radius** | **Integer**|  | [optional] [default to 500]
 **attach** | **BOOLEAN**|  | [optional] 

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pdf



# **sale_cma_pdf**
> sale_cma_pdf(property_id, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 

opts = { 
  comparable_listings: true, # BOOLEAN | 
  comparable_sales: true, # BOOLEAN | 
  cover_page: true, # BOOLEAN | 
  cover_letter: false, # BOOLEAN | 
  property_report: true, # BOOLEAN | 
  comparable_rentals: true, # BOOLEAN | 
  demographics: true, # BOOLEAN | 
  stats: true, # BOOLEAN | 
  maps: true, # BOOLEAN | 
  summary: false, # BOOLEAN | 
  radius: 500, # Integer | 
  attach: true # BOOLEAN | 
}

begin
  api_instance.sale_cma_pdf(property_id, opts)
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->sale_cma_pdf: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 
 **comparable_listings** | **BOOLEAN**|  | [optional] [default to true]
 **comparable_sales** | **BOOLEAN**|  | [optional] [default to true]
 **cover_page** | **BOOLEAN**|  | [optional] [default to true]
 **cover_letter** | **BOOLEAN**|  | [optional] [default to false]
 **property_report** | **BOOLEAN**|  | [optional] [default to true]
 **comparable_rentals** | **BOOLEAN**|  | [optional] [default to true]
 **demographics** | **BOOLEAN**|  | [optional] [default to true]
 **stats** | **BOOLEAN**|  | [optional] [default to true]
 **maps** | **BOOLEAN**|  | [optional] [default to true]
 **summary** | **BOOLEAN**|  | [optional] [default to false]
 **radius** | **Integer**|  | [optional] [default to 500]
 **attach** | **BOOLEAN**|  | [optional] 

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/pdf



# **streetview**
> streetview(property_id, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::PropertiesApi.new

property_id = 789 # Integer | 

opts = { 
  width: 789, # Integer | 
  height: 789 # Integer | 
}

begin
  api_instance.streetview(property_id, opts)
rescue Pricefinder::ApiError => e
  puts "Exception when calling PropertiesApi->streetview: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **property_id** | **Integer**|  | 
 **width** | **Integer**|  | [optional] 
 **height** | **Integer**|  | [optional] 

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/jpeg



