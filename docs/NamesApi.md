# Pricefinder::NamesApi

All URIs are relative to *https://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**names**](NamesApi.md#names) | **GET** /names | 
[**names_properties**](NamesApi.md#names_properties) | **GET** /names/properties | 
[**names_sales**](NamesApi.md#names_sales) | **GET** /names/sales | 


# **names**
> NameSearchResults names(opts)



Search for sales by owner and vendor name, as well as properties by owner name. The surname parameter can also be used to search for company/entity name.    Required Parameters:  <ul>  <li>Each request requires a state or a suburb_id.</li>  <li>Each request requires a surname/</li>  <li>If a suburb id is not supplied, then a given name is required.</li>  <ul>

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::NamesApi.new

opts = { 
  surname: "surname_example", # String | 
  given_name: "given_name_example", # String | 
  suburb_id: "suburb_id_example", # String | 
  state: "state_example", # String | 
  match_method: "startswith", # String | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.names(opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling NamesApi->names: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **surname** | **String**|  | [optional] 
 **given_name** | **String**|  | [optional] 
 **suburb_id** | **String**|  | [optional] 
 **state** | **String**|  | [optional] 
 **match_method** | **String**|  | [optional] [default to startswith]
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**NameSearchResults**](NameSearchResults.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **names_properties**
> NameSearchResults names_properties(opts)



Search for properties by owner name. The surname parameter can also be used to search for company/entity name.    Required Parameters:  <ul>  <li>Each request requires a state or a suburb_id.</li>  <li>Each request requires a surname/</li>  <li>If a suburb id is not supplied, then a given name is required.</li>  <ul>

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::NamesApi.new

opts = { 
  surname: "surname_example", # String | 
  given_name: "given_name_example", # String | 
  suburb_id: "suburb_id_example", # String | 
  state: "state_example", # String | 
  match_method: "startswith", # String | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.names_properties(opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling NamesApi->names_properties: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **surname** | **String**|  | [optional] 
 **given_name** | **String**|  | [optional] 
 **suburb_id** | **String**|  | [optional] 
 **state** | **String**|  | [optional] 
 **match_method** | **String**|  | [optional] [default to startswith]
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**NameSearchResults**](NameSearchResults.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **names_sales**
> NameSearchResults names_sales(opts)



Search for sales by owner and vendor name. The surname parameter can also be used to search for company/entity name.    Required Parameters:  <ul>  <li>Each request requires a state or a suburb_id.</li>  <li>Each request requires a surname/</li>  <li>If a suburb id is not supplied, then a given name is required.</li>  <ul>

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::NamesApi.new

opts = { 
  surname: "surname_example", # String | 
  given_name: "given_name_example", # String | 
  suburb_id: "suburb_id_example", # String | 
  state: "state_example", # String | 
  match_method: "startswith", # String | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.names_sales(opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling NamesApi->names_sales: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **surname** | **String**|  | [optional] 
 **given_name** | **String**|  | [optional] 
 **suburb_id** | **String**|  | [optional] 
 **state** | **String**|  | [optional] 
 **match_method** | **String**|  | [optional] [default to startswith]
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**NameSearchResults**](NameSearchResults.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



