# Pricefinder::AVM

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 
**price** | **Float** |  | [optional] 
**estimate_date** | **DateTime** |  | [optional] 
**confidence_score** | [**ConfidenceScore**](ConfidenceScore.md) |  | [optional] 
**estimate_range_minimum** | **Float** |  | [optional] 
**estimate_range_maximum** | **Float** |  | [optional] 
**washed_address** | [**Address**](Address.md) |  | [optional] 
**process_date** | **DateTime** |  | [optional] 
**estimate_range_midpoint** | **Float** |  | [optional] 


