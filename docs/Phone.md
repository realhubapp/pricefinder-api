# Pricefinder::Phone

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**surname** | **String** |  | [optional] 
**initials** | **String** |  | [optional] 
**address** | [**Address**](Address.md) |  | [optional] 
**number** | **String** |  | [optional] 
**do_not_call** | **BOOLEAN** |  | [optional] [default to false]
**do_not_call_wash_date** | **DateTime** |  | [optional] 


