# Pricefinder::OwnershipSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**names** | **String** |  | [optional] 
**owner_occupied** | **BOOLEAN** |  | [optional] [default to false]


