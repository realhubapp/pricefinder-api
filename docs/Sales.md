# Pricefinder::Sales

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 
**sales** | [**Array&lt;Sale&gt;**](Sale.md) |  | [optional] 


