# Pricefinder::ServletRegistration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**run_as_role** | **String** |  | [optional] 
**mappings** | **Array&lt;String&gt;** |  | [optional] 
**init_parameters** | **Hash&lt;String, String&gt;** |  | [optional] 
**name** | **String** |  | [optional] 
**class_name** | **String** |  | [optional] 


