# Pricefinder::LandDetails

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**property_area** | **Float** |  | [optional] 
**building_area** | **Float** |  | [optional] 


