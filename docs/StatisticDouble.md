# Pricefinder::StatisticDouble

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** |  | [optional] 
**value** | **Float** |  | [optional] 


