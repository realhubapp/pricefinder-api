# Pricefinder::ClassLoader

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**parent** | [**ClassLoader**](ClassLoader.md) |  | [optional] 
**idx** | **Integer** |  | [optional] 


