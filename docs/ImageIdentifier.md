# Pricefinder::ImageIdentifier

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**_self** | **String** |  | [optional] 


