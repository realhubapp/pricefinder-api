# Pricefinder::SuburbStatistics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**statistics_summary** | [**SuburbStatisticsSummary**](SuburbStatisticsSummary.md) |  | [optional] 
**sales** | [**SuburbSalesWrapper**](SuburbSalesWrapper.md) |  | [optional] 
**rentals** | [**SuburbRentalsByBedroomStatisticsWrapper**](SuburbRentalsByBedroomStatisticsWrapper.md) |  | [optional] 


