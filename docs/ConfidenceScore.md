# Pricefinder::ConfidenceScore

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | **String** |  | [optional] 
**score** | **Integer** |  | [optional] 


