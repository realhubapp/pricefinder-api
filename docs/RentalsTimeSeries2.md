# Pricefinder::RentalsTimeSeries2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**period** | [**TimeSeriesPeriod**](TimeSeriesPeriod.md) |  | [optional] 
**values** | [**Array&lt;RentalTimeSeriesValueGroup&gt;**](RentalTimeSeriesValueGroup.md) |  | [optional] 
**calculated** | **DateTime** |  | [optional] 
**description** | **String** |  | [optional] 
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 


