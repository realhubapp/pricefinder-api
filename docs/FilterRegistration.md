# Pricefinder::FilterRegistration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**servlet_name_mappings** | **Array&lt;String&gt;** |  | [optional] 
**url_pattern_mappings** | **Array&lt;String&gt;** |  | [optional] 
**init_parameters** | **Hash&lt;String, String&gt;** |  | [optional] 
**name** | **String** |  | [optional] 
**class_name** | **String** |  | [optional] 


