# Pricefinder::SalesTimeSeries

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**period** | [**TimeSeriesPeriod**](TimeSeriesPeriod.md) |  | [optional] 
**values** | [**Array&lt;SalesTimeSeriesValue&gt;**](SalesTimeSeriesValue.md) |  | [optional] 
**calculated** | **DateTime** |  | [optional] 
**description** | **String** |  | [optional] 
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 


