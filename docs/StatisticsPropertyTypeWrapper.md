# Pricefinder::StatisticsPropertyTypeWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**house** | [**SuburbStatistics**](SuburbStatistics.md) |  | [optional] 
**unit** | [**SuburbStatistics**](SuburbStatistics.md) |  | [optional] 


