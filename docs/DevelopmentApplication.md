# Pricefinder::DevelopmentApplication

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | [**Address**](Address.md) |  | [optional] 
**description** | **String** |  | [optional] 
**info_url** | **String** |  | [optional] 
**comment_url** | **String** |  | [optional] 
**location** | [**Point**](Point.md) |  | [optional] 
**date_received** | **DateTime** |  | [optional] 
**planning_authority** | [**PlanningAuthority**](PlanningAuthority.md) |  | [optional] 
**council_reference** | **String** |  | [optional] 


