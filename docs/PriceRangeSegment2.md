# Pricefinder::PriceRangeSegment2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_begin** | **Float** |  | [optional] 
**_end** | **Float** |  | [optional] 
**label** | **String** |  | [optional] 
**count** | **Integer** |  | [optional] 


