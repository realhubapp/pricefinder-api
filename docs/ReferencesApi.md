# Pricefinder::ReferencesApi

All URIs are relative to *https://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**division_properties**](ReferencesApi.md#division_properties) | **GET** /references/states/act/divisions/{division}/properties | 
[**plan_properties**](ReferencesApi.md#plan_properties) | **GET** /references/states/nsw/plans/{planType}/{planNumber}/properties | 
[**plan_properties_0**](ReferencesApi.md#plan_properties_0) | **GET** /references/states/nt/plans/{planNumber}/properties | 
[**plan_properties_1**](ReferencesApi.md#plan_properties_1) | **GET** /references/states/qld/plans/{planType}/{planNumber}/properties | 
[**plan_properties_2**](ReferencesApi.md#plan_properties_2) | **GET** /references/states/sa/plans/{planType}/{planNumber}/properties | 
[**plan_properties_3**](ReferencesApi.md#plan_properties_3) | **GET** /references/states/tas/plans/{planNumber}/properties | 
[**plan_properties_4**](ReferencesApi.md#plan_properties_4) | **GET** /references/states/vic/plans/{planType}/{planNumber}/properties | 
[**plan_properties_5**](ReferencesApi.md#plan_properties_5) | **GET** /references/states/wa/plans/{planType}/{planNumber}/properties | 
[**properties**](ReferencesApi.md#properties) | **GET** /references/states/act/divisions/{division}/sections/{section}/lots/{lot}/properties | 
[**properties_0**](ReferencesApi.md#properties_0) | **GET** /references/states/act/divisions/{division}/sections/{section}/properties | 
[**properties_1**](ReferencesApi.md#properties_1) | **GET** /references/states/act/divisions/{planNumber}/lots/{lot}/properties | 
[**properties_10**](ReferencesApi.md#properties_10) | **GET** /references/states/vic/plans/{planType}/{planNumber}/sections/{section}/lots/{lot}/properties | 
[**properties_11**](ReferencesApi.md#properties_11) | **GET** /references/states/vic/plans/{planType}/{planNumber}/sections/{section}/properties | 
[**properties_12**](ReferencesApi.md#properties_12) | **GET** /references/states/wa/plans/{planType}/{planNumber}/lots/{lot}/properties | 
[**properties_2**](ReferencesApi.md#properties_2) | **GET** /references/states/nsw/plans/{planType}/{planNumber}/lots/{lot}/properties | 
[**properties_3**](ReferencesApi.md#properties_3) | **GET** /references/states/nsw/plans/{planType}/{planNumber}/sections/{section}/lots/{lot}/properties | 
[**properties_4**](ReferencesApi.md#properties_4) | **GET** /references/states/nsw/plans/{planType}/{planNumber}/sections/{section}/properties | 
[**properties_5**](ReferencesApi.md#properties_5) | **GET** /references/states/nt/plans/{planNumber}/lots/{lot}/properties | 
[**properties_6**](ReferencesApi.md#properties_6) | **GET** /references/states/qld/plans/{planType}/{planNumber}/lots/{lot}/properties | 
[**properties_7**](ReferencesApi.md#properties_7) | **GET** /references/states/sa/plans/{planType}/{planNumber}/lots/{lot}/properties | 
[**properties_8**](ReferencesApi.md#properties_8) | **GET** /references/states/tas/plans/{planNumber}/lots/{lot}/properties | 
[**properties_9**](ReferencesApi.md#properties_9) | **GET** /references/states/vic/plans/{planType}/{planNumber}/lots/{lot}/properties | 
[**volume_folio_properties**](ReferencesApi.md#volume_folio_properties) | **GET** /references/states/vic/volumes/{volume}/folios/{folio}/properties | 
[**volume_folio_properties_0**](ReferencesApi.md#volume_folio_properties_0) | **GET** /references/states/wa/volumes/{volume}/folios/{folio}/properties | 


# **division_properties**
> PropertyIdentifiers division_properties(division, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

division = "division_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.division_properties(division, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->division_properties: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **division** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **plan_properties**
> PropertyIdentifiers plan_properties(plan_type, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

plan_type = "plan_type_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.plan_properties(plan_type, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->plan_properties: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plan_type** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **plan_properties_0**
> PropertyIdentifiers plan_properties_0(plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.plan_properties_0(plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->plan_properties_0: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **plan_properties_1**
> PropertyIdentifiers plan_properties_1(plan_type, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

plan_type = "plan_type_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.plan_properties_1(plan_type, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->plan_properties_1: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plan_type** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **plan_properties_2**
> PropertyIdentifiers plan_properties_2(plan_type, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

plan_type = "plan_type_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.plan_properties_2(plan_type, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->plan_properties_2: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plan_type** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **plan_properties_3**
> PropertyIdentifiers plan_properties_3(plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.plan_properties_3(plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->plan_properties_3: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **plan_properties_4**
> PropertyIdentifiers plan_properties_4(plan_type, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

plan_type = "plan_type_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.plan_properties_4(plan_type, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->plan_properties_4: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plan_type** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **plan_properties_5**
> PropertyIdentifiers plan_properties_5(plan_type, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

plan_type = "plan_type_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.plan_properties_5(plan_type, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->plan_properties_5: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plan_type** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties**
> PropertyIdentifiers properties(lot, section, division, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

lot = "lot_example" # String | 

section = "section_example" # String | 

division = "division_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties(lot, section, division, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->properties: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lot** | **String**|  | 
 **section** | **String**|  | 
 **division** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties_0**
> PropertyIdentifiers properties_0(section, division, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

section = "section_example" # String | 

division = "division_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties_0(section, division, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->properties_0: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **section** | **String**|  | 
 **division** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties_1**
> PropertyIdentifiers properties_1(lot, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

lot = "lot_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties_1(lot, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->properties_1: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lot** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties_10**
> PropertyIdentifiers properties_10(lot, section, plan_type, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

lot = "lot_example" # String | 

section = "section_example" # String | 

plan_type = "plan_type_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties_10(lot, section, plan_type, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->properties_10: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lot** | **String**|  | 
 **section** | **String**|  | 
 **plan_type** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties_11**
> PropertyIdentifiers properties_11(section, plan_type, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

section = "section_example" # String | 

plan_type = "plan_type_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties_11(section, plan_type, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->properties_11: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **section** | **String**|  | 
 **plan_type** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties_12**
> PropertyIdentifiers properties_12(lot, plan_type, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

lot = "lot_example" # String | 

plan_type = "plan_type_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties_12(lot, plan_type, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->properties_12: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lot** | **String**|  | 
 **plan_type** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties_2**
> PropertyIdentifiers properties_2(lot, plan_type, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

lot = "lot_example" # String | 

plan_type = "plan_type_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties_2(lot, plan_type, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->properties_2: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lot** | **String**|  | 
 **plan_type** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties_3**
> PropertyIdentifiers properties_3(lot, section, plan_type, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

lot = "lot_example" # String | 

section = "section_example" # String | 

plan_type = "plan_type_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties_3(lot, section, plan_type, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->properties_3: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lot** | **String**|  | 
 **section** | **String**|  | 
 **plan_type** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties_4**
> PropertyIdentifiers properties_4(section, plan_type, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

section = "section_example" # String | 

plan_type = "plan_type_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties_4(section, plan_type, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->properties_4: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **section** | **String**|  | 
 **plan_type** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties_5**
> PropertyIdentifiers properties_5(lot, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

lot = "lot_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties_5(lot, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->properties_5: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lot** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties_6**
> PropertyIdentifiers properties_6(lot, plan_type, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

lot = "lot_example" # String | 

plan_type = "plan_type_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties_6(lot, plan_type, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->properties_6: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lot** | **String**|  | 
 **plan_type** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties_7**
> PropertyIdentifiers properties_7(lot, plan_type, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

lot = "lot_example" # String | 

plan_type = "plan_type_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties_7(lot, plan_type, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->properties_7: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lot** | **String**|  | 
 **plan_type** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties_8**
> PropertyIdentifiers properties_8(lot, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

lot = "lot_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties_8(lot, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->properties_8: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lot** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties_9**
> PropertyIdentifiers properties_9(lot, plan_type, plan_number, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

lot = "lot_example" # String | 

plan_type = "plan_type_example" # String | 

plan_number = "plan_number_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties_9(lot, plan_type, plan_number, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->properties_9: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **lot** | **String**|  | 
 **plan_type** | **String**|  | 
 **plan_number** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **volume_folio_properties**
> PropertyIdentifiers volume_folio_properties(volume, folio, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

volume = "volume_example" # String | 

folio = "folio_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.volume_folio_properties(volume, folio, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->volume_folio_properties: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **volume** | **String**|  | 
 **folio** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **volume_folio_properties_0**
> PropertyIdentifiers volume_folio_properties_0(volume, folio, opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ReferencesApi.new

volume = "volume_example" # String | 

folio = "folio_example" # String | 

opts = { 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.volume_folio_properties_0(volume, folio, opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling ReferencesApi->volume_folio_properties_0: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **volume** | **String**|  | 
 **folio** | **String**|  | 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**PropertyIdentifiers**](PropertyIdentifiers.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



