# Pricefinder::ExtendedProperty

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 
**address** | [**ExtendedAddress**](ExtendedAddress.md) |  | [optional] 
**features** | [**ExtendedPropertyFeatures**](ExtendedPropertyFeatures.md) |  | [optional] 
**alternative_addresses** | [**Array&lt;ExtendedAddress&gt;**](ExtendedAddress.md) |  | [optional] 
**land_details** | [**ExtendedLandDetails**](ExtendedLandDetails.md) |  | [optional] 
**image** | [**ImageIdentifier**](ImageIdentifier.md) |  | [optional] 
**map** | [**PropertyMapIdentifier**](PropertyMapIdentifier.md) |  | [optional] 
**street** | [**StreetIdentifier**](StreetIdentifier.md) |  | [optional] 
**suburb** | [**SuburbIdentifier**](SuburbIdentifier.md) |  | [optional] 
**location** | [**Point**](Point.md) |  | [optional] 
**type** | **String** |  | [optional] 
**rpd** | **String** |  | [optional] 
**market_status** | [**MarketStatus**](MarketStatus.md) |  | [optional] 
**last_modified** | **DateTime** |  | [optional] 
**valuation_details** | [**ValuationDetails**](ValuationDetails.md) |  | [optional] 
**sale_history** | [**SaleHistory**](SaleHistory.md) |  | [optional] 
**listing_history** | [**ListingHistory**](ListingHistory.md) |  | [optional] 
**rental_history** | [**ListingHistory**](ListingHistory.md) |  | [optional] 
**ownership** | [**Ownership**](Ownership.md) |  | [optional] 
**phone_numbers** | [**Array&lt;Phone&gt;**](Phone.md) |  | [optional] 
**tools** | [**Tools**](Tools.md) |  | [optional] 
**_self** | **String** |  | [optional] 


