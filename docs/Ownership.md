# Pricefinder::Ownership

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**names** | **String** |  | [optional] 
**owner_occupied** | **BOOLEAN** |  | [optional] [default to false]
**owners** | [**Array&lt;Owner&gt;**](Owner.md) |  | [optional] 


