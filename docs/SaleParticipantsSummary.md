# Pricefinder::SaleParticipantsSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vendors_summary** | **String** |  | [optional] 
**purchasers_summary** | **String** |  | [optional] 


