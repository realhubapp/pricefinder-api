# Pricefinder::YearByMonthStatisticsLong

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**calculated** | **DateTime** |  | [optional] 
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 
**period** | [**TimeSeriesPeriod**](TimeSeriesPeriod.md) |  | [optional] 
**values** | [**Array&lt;StatisticLong&gt;**](StatisticLong.md) |  | [optional] 


