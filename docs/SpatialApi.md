# Pricefinder::SpatialApi

All URIs are relative to *https://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**listings**](SpatialApi.md#listings) | **GET** /spatial/listings | 
[**properties**](SpatialApi.md#properties) | **GET** /spatial/properties | 
[**rentals**](SpatialApi.md#rentals) | **GET** /spatial/rentals | 
[**sales**](SpatialApi.md#sales) | **GET** /spatial/sales | 


# **listings**
> Listings listings(opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SpatialApi.new

opts = { 
  wkt: "wkt_example", # String | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  historic: true, # BOOLEAN | 
  date_start: DateTime.parse("today-30d"), # DateTime | 
  date_end: DateTime.parse("now"), # DateTime | 
  min_price: 1.2, # Float | 
  max_price: 1.2, # Float | 
  price_gt: 1.2, # Float | 
  price_lt: 1.2, # Float | 
  limit: 100, # Integer | 
  sort: ["sort_example"] # Array<String> | can pass sort multiple times, use - to indicate descending
}

begin
  result = api_instance.listings(opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SpatialApi->listings: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wkt** | **String**|  | [optional] 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **historic** | **BOOLEAN**|  | [optional] 
 **date_start** | **DateTime**|  | [optional] [default to today-30d]
 **date_end** | **DateTime**|  | [optional] [default to now]
 **min_price** | **Float**|  | [optional] 
 **max_price** | **Float**|  | [optional] 
 **price_gt** | **Float**|  | [optional] 
 **price_lt** | **Float**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]
 **sort** | [**Array&lt;String&gt;**](String.md)| can pass sort multiple times, use - to indicate descending | [optional] 

### Return type

[**Listings**](Listings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **properties**
> Properties properties(opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SpatialApi.new

opts = { 
  wkt: "wkt_example", # String | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  limit: 100 # Integer | 
}

begin
  result = api_instance.properties(opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SpatialApi->properties: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wkt** | **String**|  | [optional] 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]

### Return type

[**Properties**](Properties.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **rentals**
> Listings rentals(opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SpatialApi.new

opts = { 
  wkt: "wkt_example", # String | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  historic: true, # BOOLEAN | 
  date_start: DateTime.parse("today-30d"), # DateTime | 
  date_end: DateTime.parse("now"), # DateTime | 
  min_price: 1.2, # Float | 
  max_price: 1.2, # Float | 
  price_gt: 1.2, # Float | 
  price_lt: 1.2, # Float | 
  limit: 100, # Integer | 
  sort: ["sort_example"] # Array<String> | can pass sort multiple times, use - to indicate descending
}

begin
  result = api_instance.rentals(opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SpatialApi->rentals: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wkt** | **String**|  | [optional] 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **historic** | **BOOLEAN**|  | [optional] 
 **date_start** | **DateTime**|  | [optional] [default to today-30d]
 **date_end** | **DateTime**|  | [optional] [default to now]
 **min_price** | **Float**|  | [optional] 
 **max_price** | **Float**|  | [optional] 
 **price_gt** | **Float**|  | [optional] 
 **price_lt** | **Float**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]
 **sort** | [**Array&lt;String&gt;**](String.md)| can pass sort multiple times, use - to indicate descending | [optional] 

### Return type

[**Listings**](Listings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



# **sales**
> Sales sales(opts)





### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::SpatialApi.new

opts = { 
  wkt: "wkt_example", # String | 
  max_area: 789, # Integer | 
  min_area: 789, # Integer | 
  max_beds: 789, # Integer | 
  min_beds: 789, # Integer | 
  max_car_parks: 789, # Integer | 
  min_car_parks: 789, # Integer | 
  max_baths: 789, # Integer | 
  min_baths: 789, # Integer | 
  property_type: "property_type_example", # String | 
  area_gt: 789, # Integer | 
  area_lt: 789, # Integer | 
  beds_gt: 789, # Integer | 
  beds_lt: 789, # Integer | 
  car_parks_gt: 789, # Integer | 
  car_parks_lt: 789, # Integer | 
  baths_gt: 789, # Integer | 
  baths_lt: 789, # Integer | 
  date_start: DateTime.parse("today-1y"), # DateTime | 
  date_end: DateTime.parse("now"), # DateTime | 
  min_price: 1.2, # Float | 
  max_price: 1.2, # Float | 
  price_gt: 1.2, # Float | 
  price_lt: 1.2, # Float | 
  limit: 100, # Integer | 
  sort: ["sort_example"] # Array<String> | can pass sort multiple times, use - to indicate descending
}

begin
  result = api_instance.sales(opts)
  p result
rescue Pricefinder::ApiError => e
  puts "Exception when calling SpatialApi->sales: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **wkt** | **String**|  | [optional] 
 **max_area** | **Integer**|  | [optional] 
 **min_area** | **Integer**|  | [optional] 
 **max_beds** | **Integer**|  | [optional] 
 **min_beds** | **Integer**|  | [optional] 
 **max_car_parks** | **Integer**|  | [optional] 
 **min_car_parks** | **Integer**|  | [optional] 
 **max_baths** | **Integer**|  | [optional] 
 **min_baths** | **Integer**|  | [optional] 
 **property_type** | **String**|  | [optional] 
 **area_gt** | **Integer**|  | [optional] 
 **area_lt** | **Integer**|  | [optional] 
 **beds_gt** | **Integer**|  | [optional] 
 **beds_lt** | **Integer**|  | [optional] 
 **car_parks_gt** | **Integer**|  | [optional] 
 **car_parks_lt** | **Integer**|  | [optional] 
 **baths_gt** | **Integer**|  | [optional] 
 **baths_lt** | **Integer**|  | [optional] 
 **date_start** | **DateTime**|  | [optional] [default to today-1y]
 **date_end** | **DateTime**|  | [optional] [default to now]
 **min_price** | **Float**|  | [optional] 
 **max_price** | **Float**|  | [optional] 
 **price_gt** | **Float**|  | [optional] 
 **price_lt** | **Float**|  | [optional] 
 **limit** | **Integer**|  | [optional] [default to 100]
 **sort** | [**Array&lt;String&gt;**](String.md)| can pass sort multiple times, use - to indicate descending | [optional] 

### Return type

[**Sales**](Sales.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json



