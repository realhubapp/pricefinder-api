# Pricefinder::ListingStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**historic** | **BOOLEAN** |  | [optional] [default to false]
**under_offer** | **BOOLEAN** |  | [optional] [default to false]


