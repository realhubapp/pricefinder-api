# Pricefinder::ImagesApi

All URIs are relative to *https://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**image**](ImagesApi.md#image) | **GET** /images/{id} | 


# **image**
> image(id, opts)



You can access this method in an image tag by appending your access_token to the query string  ie /v1/image/1?access_token=123abc

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::ImagesApi.new

id = 789 # Integer | 

opts = { 
  width: 789, # Integer | 
  height: 789 # Integer | 
}

begin
  api_instance.image(id, opts)
rescue Pricefinder::ApiError => e
  puts "Exception when calling ImagesApi->image: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  | 
 **width** | **Integer**|  | [optional] 
 **height** | **Integer**|  | [optional] 

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/gif, image/jpeg, image/png



