# Pricefinder::SalesTimeSeriesValue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **Integer** |  | [optional] 
**median** | **Float** |  | [optional] 
**high** | **Float** |  | [optional] 
**low** | **Float** |  | [optional] 
**mean** | **Float** |  | [optional] 
**total** | **Float** |  | [optional] 
**first_quartile** | **Float** |  | [optional] 
**third_quartile** | **Float** |  | [optional] 
**iq_range** | **Float** |  | [optional] 
**median_growth** | **Float** |  | [optional] 


