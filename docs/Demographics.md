# Pricefinder::Demographics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**census_topics** | [**Array&lt;CensusTopic&gt;**](CensusTopic.md) |  | [optional] 
**disclaimer** | **String** |  | [optional] 
**messages** | [**Array&lt;Message&gt;**](Message.md) |  | [optional] 


