# Pricefinder::SuggestMatch

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**property** | [**PropertyIdentifier**](PropertyIdentifier.md) |  | [optional] 
**street** | [**StreetIdentifier**](StreetIdentifier.md) |  | [optional] 
**suburb** | [**SuburbIdentifier**](SuburbIdentifier.md) |  | [optional] 
**type** | **String** |  | [optional] 
**display** | **String** |  | [optional] 
**legal_description** | **String** |  | [optional] 
**address** | [**Address**](Address.md) |  | [optional] 
**alternative_addresses** | [**Array&lt;Address&gt;**](Address.md) |  | [optional] 


