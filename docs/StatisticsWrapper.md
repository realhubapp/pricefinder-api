# Pricefinder::StatisticsWrapper

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**property_type** | [**StatisticsPropertyTypeWrapper**](StatisticsPropertyTypeWrapper.md) |  | [optional] 


