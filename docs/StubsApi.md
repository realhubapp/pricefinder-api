# Pricefinder::StubsApi

All URIs are relative to *https://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**generate_stub**](StubsApi.md#generate_stub) | **GET** /stubs/{language} | Generates a client library


# **generate_stub**
> generate_stub(language)

Generates a client library

Accepts a language and returns a set of stubs. Pricefinder is unable to provide support for this endpoint, it is provided as a convenience. There is no guarantee of backwards compatibility for this method.

### Example
```ruby
# load the gem
require 'pricefinder'

api_instance = Pricefinder::StubsApi.new

language = "language_example" # String | The target language for the client library


begin
  #Generates a client library
  api_instance.generate_stub(language)
rescue Pricefinder::ApiError => e
  puts "Exception when calling StubsApi->generate_stub: #{e}"
end
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **language** | **String**| The target language for the client library | 

### Return type

nil (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/zip, application/zip



