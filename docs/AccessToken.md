# Pricefinder::AccessToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | [optional] 
**token** | **String** |  | [optional] 
**created_date** | **DateTime** |  | [optional] 
**status** | **String** |  | [optional] 
**user_id** | **Integer** |  | [optional] 
**expires_in_seconds** | **Integer** |  | [optional] 
**expired_date** | **DateTime** |  | [optional] 
**third_party_user_id** | **Integer** |  | [optional] 
**third_party_agent_id** | **Integer** |  | [optional] 


