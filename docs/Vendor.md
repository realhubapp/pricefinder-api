# Pricefinder::Vendor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_name** | **String** |  | [optional] 
**surname** | **String** |  | [optional] 
**address** | [**Address**](Address.md) |  | [optional] 


